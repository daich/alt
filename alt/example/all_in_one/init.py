import alt.client.interface as altc


hparam_Pe = dict(
    name='Pe',
    type='FLOAT',
    valrange=dict(lower=0., upper=500.),
    warping='LINEAR')

hparam_Xa = dict(
    name='Xa',
    type='FLOAT',
    valrange=dict(lower=0., upper=1.),
    warping='LINEAR')

hparam_Pf = dict(
    name='Pf',
    type='FLOAT',
    valrange=dict(lower=0., upper=1.),
    warping='LINEAR')


def active_mixture_crosssection_fixed_phi_study():
    studySetting_dict = {
        'studyName': 'active_mixture_crosssection_fixed_phi',
        'studyAuthor': 'Chengyu',
        'maxParallelExperiments': 1000,
        'maxTrials': 1000,
        'goal': 'BINARYT',
        'hparamSetting': [hparam_Pe, hparam_Xa],
    }
    return studySetting_dict


def active_mixture_crosssection_fixed_xa_study():
    studySetting_dict = {
        'studyName': 'active_mixture_crosssection_fixed_xa',
        'studyAuthor': 'Chengyu',
        'maxParallelExperiments': 1000,
        'maxTrials': 1000,
        'goal': 'BINARYT',
        'hparamSetting': [hparam_Pe, hparam_Pf],
    }
    return studySetting_dict


def active_mixture_3d_study():
    studySetting_dict = {
        'studyName': 'active_mixture_3d',
        'studyAuthor': 'Chengyu',
        'maxParallelExperiments': 1000,
        'maxTrials': 1000,
        'goal': 'BINARYT',
        'hparamSetting': [hparam_Pe, hparam_Pf, hparam_Xa],
    }
    return studySetting_dict


def opp_alchemy_study():
    import numpy as np
    hparam_phi = dict(
        name='phi',
        type='FLOAT',
        valrange=dict(lower=0., upper=np.pi),
        warping='LINEAR')
    hparam_k = dict(
        name='k',
        type='FLOAT',
        valrange=dict(lower=4., upper=8.),
        warping='LINEAR')
    studySetting_dict = {
        'studyName': 'opp_alchemy',
        'studyAuthor': 'Chengyu',
        'maxParallelExperiments': 1000,
        'maxTrials': 1000,
        'goal': 'MINIMIZE',
        'hparamSetting': [hparam_phi, hparam_k],
    }
    return studySetting_dict


def create_study_and_log(study_dict):
    study_id = altc.createStudyFromDict(study_dict)
    with open("study_id.txt", "a") as log_file:
        msg = "{}: {}".format(study_dict['studyName'], study_id)
        log_file.write(msg)
        log_file.write('\n')


def main():
    study_dicts = [
        active_mixture_crosssection_fixed_phi_study(),
        active_mixture_crosssection_fixed_xa_study(),
        active_mixture_3d_study(),
        opp_alchemy_study(),
    ]
    for study in study_dicts:
        study['studyName'] = "Prod_" + study['studyName']
        create_study_and_log(study)


if __name__ == "__main__":
    main()
