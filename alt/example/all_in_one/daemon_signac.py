import click
import hoomd
import logging
import signac

import alt.client.interface as altc
from alt.example.all_in_one.job_script import operations, opp_operations


_logger = logging.getLogger(__name__)
_logger.setLevel(logging.INFO)


def init_hoomd(gpu_id):
    gpu_id = int(gpu_id)
    if hoomd.context.exec_conf is None:
        txt = '--mode=gpu'
        if gpu_id is not None:
            assert type(gpu_id) == int
            txt += ' --gpu={}'.format(gpu_id)
        hoomd.context.initialize(txt)


@click.command()
@click.option('--mode', prompt='0. Fixed phi (packing fraction) at 0.6\n' +
                               '1. Fixed Xa (active fraction) at 0.5\n' +
                               '2. All three variable can change, Pe, Xa, Phi.\n' +
                               '3. Short OPP experiment.\n' +
                               '4. Full OPP experiment.')
@click.option('--study_id', prompt='The study ID for ALT service.')
@click.option('--gpu_id', prompt='The GPU ID.')
@click.option('--max_jobs', prompt='Maximum number of jobs per client.')
def run_daemon(mode, study_id, gpu_id, max_jobs):
    init_hoomd(gpu_id)
    mode = int(mode)
    max_jobs = int(max_jobs)
    assert mode in [0, 1, 2, 3]

    project = signac.get_project()
    for i in range(max_jobs):
        exp_id = altc.createExperiment(study_id)
        exp = altc.infoExperiment(study_id, exp_id)
        hparam_dict = altc.extract_hparamValue_to_dict(exp)
        _logger.info("New experiment with dict:")
        _logger.info(hparam_dict)

        statepoint = fill_statepoint(hparam_dict, mode)
        job = project.open_job(statepoint)

        if mode not in [3, 4]:
            result = operations.run_dynamics(job)
        else:
            if_short = mode == 3
            result = opp_operations.opp_dynamics(job, test=if_short)
        altc.finishExperimentWithMetric(study_id, exp_id, result)
        _logger.info("Experiment finished with metric:")
        _logger.info(result)
        _logger.info("--" * 10)


def fill_statepoint(hparam_dict, mode):
    if mode == 3:
        return hparam_dict

    statepoint_template = {
        'active_epsilon_ratio': 24,        # following Michael Cates 2013 Soft Matters
        'rot_diff':     0,                 # rotational diffusion
        'random_seed':  168,
        'gamma':        1,
        'epsilon':      1,
        'act_mag':      50,
        'noiseless_t':  True,
        'noiseless_r':  True,
        'num_dumps': 20,
        'num_tau': 20,
        'nside':        100,
    }
    if mode == 0:
        statepoint_template['Pf'] = 0.6
    if mode == 1:
        statepoint_template['Xa'] = 0.5

    # If packing fraction is 0 there will be exception, so we use a small value.
    result = {**hparam_dict, **statepoint_template}
    if result['Pf'] == 0:
        result['Pf'] = 0.01
    return result


if __name__ == "__main__":
    run_daemon()
