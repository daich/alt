import click

import alt.client.interface as altc
from alt.example.all_in_one.compute import some_function


@click.command()
@click.option('--study_id', prompt='study_id')
def run_daemon(study_id):
    for i in range(10):
        exp_id = altc.createExperiment(study_id)
        exp = altc.infoExperiment(study_id, exp_id)
        hparam_dict = altc.extract_hparamValue_to_dict(exp)

        result = some_function(hparam_dict)

        altc.finishExperimentWithMetric(study_id, exp_id, result)


if __name__ == "__main__":
    run_daemon()
