from random import randint
from time import sleep


def some_function(hparam_dict):
    print(str(hparam_dict) + ',')
    pe = hparam_dict['Pe']
    xa = hparam_dict['Xa']

    sleep(randint(0, 3))

    return +1 if pe * xa > 150 else -1
