# STD LIB deps
from math import floor
import numpy as np
import numpy.random

# Glotzer LIB deps
import freud
from freud.locality import NearestNeighbors
import gsd
import gsd.fl
import gsd.hoomd


class Box(object):

    def __init__(self, lx, ly):
        self.Lx = lx
        self.Ly = ly
        self.Lz = 0
        self.xy = 0
        self.xz = 0
        self.yz = 0
        self.dimensions = 2


def calculate_num_of_actives(N, Xa):
    return floor(N * Xa)


def gsd_generate_pointpar(nside, packing_fraction, Xa, mass, diam, random_seed,
                          moi):
    '''
        packing_fraction: float in [0, 1]
        Xa: float in [0, 1], the active particle fraction
    '''
    assert 0 <= packing_fraction <= 1
    assert 0 <= Xa <= 1
    print("particle diameter")
    print(diam)
    # 2D generation of random lattice of point particles
    L = np.sqrt(np.pi * diam**2 * nside**2 / 4.0 / packing_fraction)
    Lz = 0.002
    numpy.random.seed(seed=random_seed)

    s = gsd.hoomd.Snapshot()
    N = nside**2

    s.configuration.dimensions = 2

    s.particles.N = N
    s.particles.diameter = [diam for i in range(N)]
    s.particles.mass = [mass for i in range(N)]

    number_of_actives = calculate_num_of_actives(N, Xa)
    s.particles.types = ['active', 'passive']
    s.particles.typeid = [0 if i < number_of_actives else 1 for i in range(N)]

    s.particles.moment_inertia = [[moi, moi, moi] for i in range(N)]
    positions = numpy.random.uniform(size=(N, 2)) * L - L / 2.0
    positions = np.hstack((positions, np.zeros((N, 1))))

    s.particles.position = positions
    s.configuration.box = [L, L, Lz, 0, 0, 0]
    gsd.hoomd.create(name='init.gsd', snapshot=s)


def get_num_neigh(box, pos):
    diam = 0.48
    r_cut = diam * 2**(1 / 6)
    nn = NearestNeighbors(r_cut, 6, strict_cut=True)
    nn.compute(box, pos, pos)
    nl = nn.getNeighborList()
    iui32 = np.iinfo(np.uint32)
    return np.sum(nl != iui32.max, axis=1)


def analyze_neighbor(snapshot):
    pos = snapshot.particles.position
    box = freud.box.Box(snapshot.box.Lx, snapshot.box.Ly, 0, 0, 0, 0, is2D=True)
    num_neigh = get_num_neigh(box, pos)

    particle_fraction_with_neigh = []
    for i in range(6 + 1):
        particle_fraction_with_neigh.append(
            np.count_nonzero(num_neigh == i) / len(num_neigh))
    return particle_fraction_with_neigh


def phase_decision(neigh_fraction_lst):
    six_neigh_frac = neigh_fraction_lst[-1]
    if six_neigh_frac > 0.15:
        return 1
    else:
        return -1


def run_dynamics(job, test=False, args=None):
    import hoomd
    import hoomd.md
    import hoomd.group

    hoomd.context.initialize('')
    with job:
        statepoint = job.statepoint()

        # Fundamental quantities from statepoint's init params.
        N = statepoint['nside']**2
        radius = statepoint['epsilon'] * statepoint[
            'active_epsilon_ratio'] / 2. / statepoint['act_mag']
        diam = radius * 2.
        T = diam * statepoint['act_mag'] / statepoint['Pe']
        gamma = statepoint['gamma']
        gamma_r = (float((diam)**2 * statepoint['gamma'] / 3.0))

        # Calculate inertia params, everything will not be used except D_r
        # as we use Brownian Dynamics.
        mass = 1
        moi = 1
        D_r = 3 * T / gamma / diam**2

        # Init with snapshot.
        gsd_generate_pointpar(statepoint['nside'], statepoint['Pf'], statepoint['Xa'],
                              mass, diam, statepoint['random_seed'], moi)
        system = hoomd.init.read_gsd('init.gsd')
        nl = hoomd.md.nlist.cell()
        group_active = hoomd.group.type('active')

        # Interaction.
        lj = hoomd.md.pair.lj(r_cut=3.0, nlist=nl)
        lj.set_params(mode="shift")
        lj.pair_coeff.set(
            'active',
            'active',
            epsilon=statepoint['epsilon'],
            sigma=diam,
            r_cut=diam * 2**(1. / 6.))
        lj.pair_coeff.set(
            'active',
            'passive',
            epsilon=statepoint['epsilon'],
            sigma=diam,
            r_cut=diam * 2**(1. / 6.))
        lj.pair_coeff.set(
            'passive',
            'passive',
            epsilon=statepoint['epsilon'],
            sigma=diam,
            r_cut=diam * 2**(1. / 6.))

        # HOOMD version 2.3 - active force API.
        f_lst = []
        number_of_actives = calculate_num_of_actives(N, statepoint['Xa'])
        print("Number of active particles {}/{}".format(number_of_actives, N))
        for _ in range(number_of_actives):
            tmp_angle = np.pi * 2 * np.random.uniform()
            f_lst.append((statepoint['act_mag'] * np.cos(tmp_angle),
                          statepoint['act_mag'] * np.sin(tmp_angle), 0))

        active_force = hoomd.md.force.active(
            statepoint['random_seed'],
            group=group_active,
            f_lst=f_lst,
            orientation_link=False,
            rotation_diff=D_r,
            orientation_reverse_link=True)

        # # store the direction of the active force.
        # force_director = np.ndarray((N, 3))
        # for i in range(N):
        #     force_director[i, :] = np.array(f_lst[i])
        # np.save("force_director.npy", force_director)

        # Dimensionless time.
        tau = diam**2 / (T / statepoint['gamma'])
        dt = 1e-5 * tau * (radius**2 / diam**2)

        dt_factor = 1
        if 'dt_factor' in statepoint:
            dt_factor = statepoint['dt_factor']
        dt *= dt_factor

        num_timesteps_pertau = tau / dt
        num_timesteps = num_timesteps_pertau * statepoint['num_tau']
        print("Num steps: " + str(num_timesteps))

        # Turning off aniso to get point particle behavior.
        # Brownian rotational diffusion but langevin translational motion.
        hoomd.md.integrate.mode_standard(aniso=False, dt=dt)

        all_group = hoomd.group.all()
        dynamics = hoomd.md.integrate.brownian(
            kT=T,
            group=all_group,
            seed=statepoint['random_seed'],
            noiseless_t=statepoint['noiseless_t'],
            noiseless_r=statepoint['noiseless_r'])
        dynamics.set_gamma('A', gamma)
        dynamics.set_gamma_r('A', gamma_r)

        # Initialization scheme will contain overlapping particles,
        # we use DPD to relax the system into no overlapping.
        active_force.disable()
        lj.disable()
        initializing_force = hoomd.md.pair.dpd_conservative(
            r_cut=diam * 2**(1. / 6.), nlist=nl)
        initializing_force.pair_coeff.set('active', 'active', A=100.0)
        initializing_force.pair_coeff.set('active', 'passive', A=100.0)
        initializing_force.pair_coeff.set('passive', 'passive', A=100.0)
        hoomd.run(2e5)

        # Convert back to real dynamics to run.
        active_force.enable()
        lj.enable()
        initializing_force.disable()

        # Assume always one dump is one tau, this must be enforced in init.py !!!
        assert statepoint['num_dumps'] == statepoint['num_tau']

        try:
            steps_per_dump = int(num_timesteps / statepoint['num_dumps'])

            gsd_video = hoomd.dump.gsd(
                filename='dump.gsd',
                phase=0,
                group=all_group,
                static=['attribute', 'topology'],
                period=steps_per_dump,
                overwrite=True)

            # For brownian, run additional 10 tau to ensure phase separation
            # hoomd.run(steps_100_tau // 10)
            run_len_in_tau = statepoint['num_dumps']
            if test:
                run_len_in_tau = 10

            phase_history = []
            for stage in range(run_len_in_tau):
                hoomd.run(steps_per_dump)
                snap = system.take_snapshot()
                neigh_fraction = analyze_neighbor(snap)

                # If phase separate immediately returns, otherwise wait till last stage.
                print("Stage {}:".format(stage))
                print(neigh_fraction)
                phase_history.append(phase_decision(neigh_fraction))

                # Only emit okay for consistent clustering at least 5 tau.
                if len(phase_history) >= 5 and sum(phase_history[-5:]) == 5:
                    return 1

            return -1

        except hoomd.WalltimeLimitReached:
            pass
        finally:
            job.document['num_steps'] = hoomd.get_step()
            gsd_video.write_restart()
