def opp_dynamics(job, test=False):

    with job:
        statepoint = job.statepoint()
        k = statepoint['k']
        phi = statepoint['phi']

        import hoomd
        from hoomd import md
        from hoomd import WalltimeLimitReached

        r_cut = 3.0
        N = 4096
        randomstep = 500000
        coolingsteps = 20000000

        if test:
            randomstep /= 5
            coolingsteps /= 5

        dump_period = 100000

        temp_s1 = 3.0
        temp_s2 = 0.5
        temp_e = 0.1

        hoomd.context.initialize("")
        collins_gsd_fn = '/home/daich/singularity/alt/alt/example/all_in_one/opp_init.gsd'
        _ = hoomd.init.read_gsd(filename=collins_gsd_fn)

        nl = md.nlist.cell()
        opp_shift = md.pair.OPP(r_cut=r_cut, nlist=nl)
        opp_shift.pair_coeff.set('A', 'A', k=k, phi=phi, delta=1)

        all_group = hoomd.group.all()
        temp_ramp = hoomd.variant.linear_interp([(0, temp_s1),
                                                 (randomstep, temp_s2),
                                                 (randomstep + coolingsteps, temp_e),
                                                 (randomstep + coolingsteps, temp_e)],
                                                zero=0)

        md.update.zero_momentum(period=dump_period)
        md.integrate.mode_standard(dt=0.005)
        md.integrate.langevin(group=all_group,
                              kT=temp_ramp,
                              seed=111)

        logger = hoomd.analyze.log(filename="data.log",
                                   quantities=['potential_energy',
                                               'kinetic_energy',
                                               'pressure',
                                               'temperature'],
                                   period=dump_period, overwrite=False, phase=0)
        _ = hoomd.dump.gsd('dump.gsd',
                           period=dump_period,
                           group=all_group,
                           overwrite=False,
                           truncate=False,
                           phase=0)
        hoomd.meta.dump_metadata(filename='meta.json')

        try:
            hoomd.run(randomstep + coolingsteps)

        except WalltimeLimitReached:
            pass

        energy = logger.query('potential_energy') / N

    return energy
