import signac

from alt.example.all_in_one.daemon_signac import fill_statepoint
from alt.example.all_in_one.job_script.operations import run_dynamics
from alt.example.all_in_one.job_script.opp_operations import opp_dynamics


def test_active_dynamics():
    project = signac.get_project()
    statepoint = {'Pe': 200, 'Xa': 0.285}
    statepoint = fill_statepoint(statepoint)
    job = project.open_job(statepoint)

    result = run_dynamics(job)
    print(result)


def test_opp_dynamics():
    project = signac.get_project()
    statepoint = {'k': 0.2, 'phi': 0.7}
    job = project.open_job(statepoint)

    result = opp_dynamics(job, test=True)
    print(result)


if __name__ == "__main__":
    test_opp_dynamics()
