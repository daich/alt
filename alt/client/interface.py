''' The user interface of Client ALT.
'''

from protobuf_to_dict import dict_to_protobuf

from alt.proto.python_build import (
    study_pb2,
    experiment_pb2,
    metric_pb2,
)
from alt.client.interface_base import (
    RpcCreateStudy, RpcInfoStudy, RpcStopStudy, RpcShouldStopStudy,
    RpcCreateExperiment, RpcCreateExperimentWithHparam, RpcInfoExperiment,
    RpcStopExperiment, RpcUpdateMetric, RpcFinishExperimentWithMetric)


# -------------------- Study -------------------- #


def createStudyFromDict(studySetting_dict, test_stub=None):
    studySetting = dict_to_protobuf(
        study_pb2.StudySetting, values=studySetting_dict)
    return createStudy(studySetting, test_stub=test_stub)


def createStudy(studySetting, test_stub=None):
    request = studySetting
    response = RpcCreateStudy.rpc_call(request, test_stub)
    study_id = response.studyID
    return study_id


def infoStudy(study_id, test_stub=None):
    request = study_pb2.StudyID(studyID=study_id)
    response = RpcInfoStudy.rpc_call(request, test_stub)
    return response


def stopStudy(study_id, test_stub=None):
    request = study_pb2.StudyID(studyID=study_id)
    RpcStopStudy.rpc_call(request, test_stub)
    return True


def shouldStopStudy(study_id, test_stub=None):
    request = study_pb2.StudyID(studyID=study_id)
    response = RpcShouldStopStudy.rpc_call(request, test_stub)
    return response.bool


# -------------------- Experiment -------------------- #


def createExperiment(study_id, test_stub=None):
    request = study_pb2.StudyID(studyID=study_id)
    response = RpcCreateExperiment.rpc_call(request, test_stub)
    return response.experimentID


def createExperimentWithHparamFromDict(expSetting_dict, test_stub=None):
    expSetting = dict_to_protobuf(
        experiment_pb2.ExperimentWithHparam, values=expSetting_dict)
    return createExperimentWithHparam(expSetting, test_stub=test_stub)


def createExperimentWithHparam(expSetting, test_stub=None):
    request = expSetting
    response = RpcCreateExperimentWithHparam.rpc_call(request, test_stub)
    exp_id = response.experimentID
    return exp_id


def infoExperiment(study_id, experiment_id, test_stub=None):
    request = experiment_pb2.ExperimentID(
        studyID=study_id, experimentID=experiment_id)
    response = RpcInfoExperiment.rpc_call(request, test_stub)
    return response


def stopExperiment(study_id, experiment_id, test_stub=None):
    request = experiment_pb2.ExperimentID(
        studyID=study_id, experimentID=experiment_id)
    RpcStopExperiment.rpc_call(request, test_stub)
    return True


# -------------------- Metric -------------------- #


def updateMetric(study_id,
                 exp_id,
                 metric_value,
                 metric_value_var=0,
                 note="",
                 expAliasID=None,
                 test_stub=None):
    metric = metric_pb2.Metric(value=metric_value,
                               value_var=metric_value_var,)
    request = metric_pb2.ExpMetric(
        studyID=study_id,
        experimentID=exp_id,
        metric=metric,
    )
    if note:
        request.note = note
    if expAliasID:
        request.experimentAliasID = expAliasID
    RpcUpdateMetric.rpc_call(request, test_stub)
    return True


def updateMetricBinaryTReplica(study_id,
                               exp_id,
                               binary_result_list,
                               note="",
                               expAliasID=None,
                               test_stub=None):
    for i in binary_result_list:
        if i not in [1, -1]:
            raise ValueError("The experiment result could only be binary int" +
                             "that takes value of +1 or -1.")

    metric_value, metric_var = binomial_prob_estimator(binary_result_list)
    return updateMetric(study_id,
                        exp_id,
                        metric_value,
                        metric_var,
                        note=note,
                        expAliasID=expAliasID,
                        test_stub=test_stub)


def finishExperimentWithMetric(study_id,
                               exp_id,
                               metric_value,
                               metric_value_var=0,
                               note="",
                               expAliasID=None,
                               test_stub=None):
    metric = metric_pb2.Metric(value=metric_value,
                               value_var=metric_value_var)
    request = metric_pb2.ExpMetric(
        studyID=study_id,
        experimentID=exp_id,
        metric=metric,
    )
    if note:
        request.note = note
    if expAliasID:
        request.experimentAliasID = expAliasID
    RpcFinishExperimentWithMetric.rpc_call(request, test_stub)
    return True


def finishExperimentWithBinaryTReplica(study_id,
                                       exp_id,
                                       binary_result_list,
                                       note="",
                                       expAliasID=None,
                                       test_stub=None):
    for i in binary_result_list:
        if i not in [1, -1]:
            raise ValueError("The experiment result could only be binary int" +
                             "that takes value of +1 or -1.")

    metric_value, metric_var = binomial_prob_estimator(binary_result_list)
    return finishExperimentWithMetric(study_id,
                                      exp_id,
                                      metric_value,
                                      metric_var,
                                      note=note,
                                      expAliasID=expAliasID,
                                      test_stub=test_stub)


# -------------------- Utils -------------------- #


def extract_hparamValue_to_dict(exp):
    valueList = exp.hparamValue
    valueDict = {hparam.name: hparam.value for hparam in valueList}
    return valueDict


def binomial_prob_estimator(binary_result_list):
    _check_binary(binary_result_list)
    # Avg Phase is between [-1, 1].
    phase = sum(binary_result_list) / len(binary_result_list)

    # Probability is between [0, 1].
    prob = (phase + 1) / 2.
    var_estimator = 4 * prob * (1 - prob) / len(binary_result_list)
    return phase, var_estimator


def _check_binary(binary_result_list):
    for i in binary_result_list:
        if i not in [1, -1]:
            raise ValueError("Only binary results of type int 1 and -1 accepted.")
