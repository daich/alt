from alt.proto.python_build.hparam_pb2 import WarpingType, HparamType

LINEAR = WarpingType.Value('LINEAR')
LOG = WarpingType.Value('LOG')

FLOAT = HparamType.Value('FLOAT')
INT = HparamType.Value('INT')
DISCRETE = HparamType.Value('DISCRETE')
