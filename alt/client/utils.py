from alt.proto.python_build.status_pb2 import HparamType

HPARAM_TYPE_CONVERTER = {
    HparamType.Value('FLOAT'): float,
    HparamType.Value('DISCRETE'): float,
    HparamType.Value('INT'): int,
}


def parse_hparam(hparamValueList):
    statepoint = dict()
    for hparam in hparamValueList:
        type_converter = HPARAM_TYPE_CONVERTER[hparam.type]
        statepoint[hparam.name] = type_converter(hparam.value)
    return statepoint
