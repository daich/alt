import click
import datetime
import logging
import yaml

from alt.client import interface


_logger = logging.getLogger("Active Learning Toolbox CLI")
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)


# -------------------- Study -------------------- #


@click.group()
def study():
    pass


@study.command()
@click.option('--config', required=True, help='Config yaml file path.')
def create(config):
    with open(config, 'r') as fn:
        studySetting_dict = yaml.load(fn)

    study_id = interface.createExperimentFromDict(studySetting_dict)
    _logger.info(study_id)
    _logger.info("Study created successfully.")

    with open("active_learning_toolbox_history.txt", 'a') as fn_out:
        msg = "{}: Created study {}."
        msg = msg.format(datetime.datetime.utcnow(), study_id)
        fn_out.write()


@study.command()
@click.argument('studyID')
def info(studyid):
    obj = interface.infoStudy(studyid)
    _logger.info(obj)
    _logger.info("Study info queried successfully.")


@study.command()
@click.argument('studyID')
def stop(studyid):
    obj = interface.stopStudy(studyid)
    _logger.info(obj)
    _logger.info("Study stopped successfully.")


# -------------------- Experiment -------------------- #


@click.group()
def experiment():
    pass


@experiment.command()  # noqa: F811
@click.argument('studyID')
@click.argument('experimentID')
def info(studyid, experimentid):
    obj = interface.infoExperiment(studyid, experimentid)
    _logger.info(obj)
    _logger.info("Experiment info queried successfully.")


@experiment.command()  # noqa: F811
@click.argument('studyID')
@click.argument('experimentID')
def stop(studyid, experimentid):
    obj = interface.stopExperiment(studyid, experimentid)
    _logger.info(obj)
    _logger.info("Experiment stopped successfully.")


# -------------------- Main -------------------- #


@click.group()
def entry_point():
    pass


if __name__ == '__main__':
    entry_point.add_command(study)
    entry_point.add_command(experiment)
    entry_point()
