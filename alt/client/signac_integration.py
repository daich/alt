import datetime
import fcntl  # For file locking.


def _safe_logging(str_to_append, safe_log_fn):
    SAFE_LOGGING_PREFIX = 'Safe Logger {}: '.format(datetime.datetime.utcnow())

    with open(safe_log_fn, "a") as g:
        fcntl.flock(g, fcntl.LOCK_EX)
        g.write(SAFE_LOGGING_PREFIX + str_to_append)
        fcntl.flock(g, fcntl.LOCK_UN)


class ALTClientProjectBase(object):
    """ Base class of all ALT Client Project for easy Signac Integration. """

    def __init__(self, study_id):
        raise NotImplementedError

    def operation_daemon(self):
        raise NotImplementedError

    def operation_main(self, hparamaList):
        raise NotImplementedError
