import grpc

from alt.client.setting import SERVER_URL
from alt.server.core.external_exception import ExtException
from alt.proto.python_build import service_pb2_grpc


class ClientInterfaceBase(object):
    '''
    This abstraction provides the rpc call abstraction for all client interface functions.
    This is needed for a unified interface code regardless of the implementation of how the
    rpc is done and how channel / stub are managed. In testing, we only need to plugin the
    pytest-grpc generated test_stub.
    '''

    method_name = ""

    @classmethod
    def rpc_call(cls, request, test_stub=None):
        if type(request) in [dict, list, set, str]:
            raise ValueError('RPC must be called with Protobuf Message Type.')

        try:
            if test_stub:
                test_rpc_method = cls.gen_rpc_method(test_stub)
                response = test_rpc_method(request)
            else:
                with grpc.insecure_channel(SERVER_URL) as channel:
                    stub = service_pb2_grpc.ALTStub(channel)
                    rpc_method = cls.gen_rpc_method(stub)
                    response = rpc_method(request)
            return response

        except grpc.RpcError as err:
            cls.raise_to_user(err.code(), err.details())

    @classmethod
    def raise_to_user(cls, status_code, details):
        ExtException.raise_from_context(status_code, details)

    @classmethod
    def gen_rpc_method(cls, stub):
        return getattr(stub, cls.method_name, None)


# -------------------- Study -------------------- #


class RpcCreateStudy(ClientInterfaceBase):
    method_name = "createStudy"


class RpcInfoStudy(ClientInterfaceBase):
    method_name = "infoStudy"


class RpcStopStudy(ClientInterfaceBase):
    method_name = "stopStudy"


class RpcShouldStopStudy(ClientInterfaceBase):
    method_name = "shouldStopStudy"


# -------------------- Experiment -------------------- #


class RpcCreateExperiment(ClientInterfaceBase):
    method_name = "createExperiment"


class RpcCreateExperimentWithHparam(ClientInterfaceBase):
    method_name = "createExperimentWithHparam"


class RpcInfoExperiment(ClientInterfaceBase):
    method_name = "infoExperiment"


class RpcStopExperiment(ClientInterfaceBase):
    method_name = "stopExperiment"


# -------------------- Metric -------------------- #


class RpcUpdateMetric(ClientInterfaceBase):
    method_name = "updateMetric"


class RpcFinishExperimentWithMetric(ClientInterfaceBase):
    method_name = "finishExperimentWithMetric"
