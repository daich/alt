from functools import wraps
import logging
from logging.handlers import TimedRotatingFileHandler

logname = "alt_service.log"
handler = TimedRotatingFileHandler(logname, when="midnight", interval=1)
handler.suffix = "%Y%m%d"

_logger = logging.getLogger("Active Learning Toolbox Server")
_logger.addHandler(handler)


def log_request(logger=_logger):

    def decorator(func):

        @wraps(func)
        def wrapper(*args, **kwargs):
            request = args[1]
            logger.debug(str(request))
            result = func(*args, **kwargs)
            logger.debug()
            return result

        return wrapper

    return decorator
