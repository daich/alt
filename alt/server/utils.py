import sys


def is_called_from_test():
    return hasattr(sys, '_called_from_test')


def pytest_opt_plot_to_gui():
    return hasattr(sys, '_pytest_opt_plot_to_gui')
