import logging
import time
import os
from pymongo import MongoClient

_logger = logging.getLogger("Server Database Backend")
logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)


class DatabaseBackendBase(object):

    def get_client(self):
        raise NotImplementedError

    def get_collection(self, client, collection_name):
        raise NotImplementedError

    def get_exp_collection(self, client, study_id):
        raise NotImplementedError

    def get_all_exp_in_study(self, client, study_id):
        exp_collection = self.get_exp_collection(client, study_id)
        return list(exp_collection.find())


class MongoDBBase(DatabaseBackendBase):
    PASSWORD = ""
    ATLAS_ADDR = ""
    ATLAS_ADDR = ""

    def _get_study_db(self, client):
        raise NotImplementedError

    def _get_exp_db(self, client):
        raise NotImplementedError

    def teardown(self):
        raise NotImplementedError

    def setup(self):
        raise NotImplementedError

    def get_collection(self, client, collection_name):
        study_db = self._get_study_db(client)

        collection = getattr(study_db, collection_name, None)
        if not collection:
            raise AttributeError(
                "{} is not in database.".format(collection_name))
        return collection

    def get_exp_collection(self, client, study_id):
        exp_db = self._get_exp_db(client)

        collection = getattr(exp_db, study_id, None)
        if collection is None:
            raise AttributeError(
                'No experiment collection is found for study {}.'.format(
                    study_id))
        return collection


class ProductionMongoDB(MongoDBBase):
    PASSWORD = "pineapple"
    ATLAS_ADDR = "mongodb://alt_server:{}@glotzerlabmongo-shard-00-00-vjefj.mongodb.net:27017,glotzerlabmongo-shard-00-01-vjefj.mongodb.net:27017,glotzerlabmongo-shard-00-02-vjefj.mongodb.net:27017/test?ssl=true&replicaSet=GlotzerLabMongo-shard-0&authSource=admin&retryWrites=true" # noqa
    ATLAS_ADDR = ATLAS_ADDR.format(PASSWORD)

    def _get_study_db(self, client):
        return client['alt']

    def _get_exp_db(self, client):
        return client['alt_experiments']

    def setup(self):
        _logger.info("Production MongoDB initializing.")
        pass

    def teardown(self):
        _logger.info("Production MongoDB shutting down.")
        pass


class TestingMongoDB(MongoDBBase):
    PASSWORD = "pineapple"
    ATLAS_ADDR = "mongodb+srv://pytest_alt:{}@cluster1-5bl4e.mongodb.net/test?retryWrites=true"
    ATLAS_ADDR = ATLAS_ADDR.format(PASSWORD)

    def __init__(self):
        self.timestamp = int(time.time())

        # Suffix was used for deduplication. In parallel testing pytest-xdist, new process was
        # created for each worker, we use timestamp as well as pid to make sure teardown of one
        # db backend would not destroy a same name db.
        self.db_suffix = "_test_{}_{}".format(os.getpid(), self.timestamp)
        self.study_db_name = 'alt' + self.db_suffix
        self.exp_db_name = 'alt_experiments' + self.db_suffix

        self.setup()

    def __del__(self):
        self.teardown()

    def _get_study_db(self, client):
        return client[self.study_db_name]

    def _get_exp_db(self, client):
        return client[self.exp_db_name]

    def setup(self):
        _logger.info("Testing MongoDB initializing with suffix {}.".format(
            self.db_suffix))

    def teardown(self):
        _logger.info("Testing MongoDB shutting down with suffix {}.".format(
            self.db_suffix))
        with MongoClient(TestingMongoDB.ATLAS_ADDR) as client:
            db_names = client.database_names()
            pytest_residuals = [
                name for name in db_names if self.db_suffix in name
            ]
            for resi in pytest_residuals:
                client.drop_database(resi)
