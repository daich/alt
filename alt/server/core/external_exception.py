""" Exceptions that can be broadcasted to user clients through grpc's context object.
    Important: as pytest only supports raise exception class rather than instance, we need
    introspective capability for the base class to recover the server exception from client.
    To recover the original ext_exc, we require all subclasses of ExtException to have fixed
    static message and status code.
"""

import grpc


class ExtException(Exception):
    """ The base class for all exceptions to be exposed to the clients.
    """
    grpc_status_code = grpc.StatusCode.UNKNOWN
    msg = ""

    @classmethod
    def raise_from_context(cls, status_code, msg):
        for subcls in cls.get_subclasses():
            if (subcls.grpc_status_code == status_code) and (subcls.msg == msg):
                raise subcls

    @classmethod
    def get_subclasses(cls):
        for subclass in cls.__subclasses__():
            yield from subclass.get_subclasses()
            yield subclass


class StudyAlreadyExistError(ExtException):
    grpc_status_code = grpc.StatusCode.ALREADY_EXISTS
    msg = "The study already exists."


class StudyNotFoundError(ExtException):
    grpc_status_code = grpc.StatusCode.NOT_FOUND
    msg = "The study was not found."


class ExperimentAlreadyExistError(ExtException):
    grpc_status_code = grpc.StatusCode.ALREADY_EXISTS
    msg = "The experiment already exists."


class ExperimentNotFoundError(ExtException):
    grpc_status_code = grpc.StatusCode.NOT_FOUND
    msg = "The experiment was not found."


class ExperimentAlreadyFinishedError(ExtException):
    grpc_status_code = grpc.StatusCode.FAILED_PRECONDITION
    msg = "The experiment has already finished. Metrics can't be changed or updated any more."


class HparamNameDuplicateError(ExtException):
    grpc_status_code = grpc.StatusCode.INVALID_ARGUMENT
    msg = """ The hyperparameters must have distinct names
              to ensure the correct serialization order in server.
          """


class HparamValueInvalidError(ExtException):
    grpc_status_code = grpc.StatusCode.INVALID_ARGUMENT
    msg = """ The Hparam provided must match all the requirements as
              provided in studySetting.hparamSettingList. Use RPC
              infoStudy to query the studySetting and validate your
              hparamValueList.
          """


class IdNotValidError(ExtException):
    grpc_status_code = grpc.StatusCode.INVALID_ARGUMENT
    msg = """ The ids provided are not valid. All ids should be
              12-byte input or a 24-character hex string according to
              the server's database backend implementation (MongoDB).
          """


class ConstraintSyntaxError(ExtException):
    grpc_status_code = grpc.StatusCode.INVALID_ARGUMENT
    msg = """ The constraint string should not contain double quote or other
              invalid character.
          """
