from itertools import chain
import numpy as np

from alt.client.types import LINEAR, FLOAT
from alt.server.core.warping import LinearWarping


class RegularExpData(object):

    def __init__(self, pending_X, finished_X, finished_Y, finished_Y_var=None):
        self.pending_X = pending_X
        self.finished_X = finished_X
        self.finished_Y = finished_Y
        self.finished_Y_var = finished_Y_var

        assert pending_X.warping_list is finished_X.warping_list
        self.warping_list = pending_X.warping_list

    @classmethod
    def from_exps(cls, exps, studySetting):
        pending, running, finished = cls._filter_exps(exps)

        # Here pending_X would include both pending and running experiments for now.
        pending_X, finished_X, finished_Y, finished_Y_var = \
            cls._extract_hparamValue_from_exps(
                studySetting.hparamSetting,
                pending,
                running,
                finished
            )
        return cls(pending_X, finished_X, finished_Y, finished_Y_var)

    def num_scheduled(self, logger=None):
        num_exps = 0
        num_exps += len(self.finished_Y) if self.finished_Y is not None else 0
        num_exps += self.pending_X.shape[0] if not self.pending_X.empty else 0
        if logger:
            logger.info("Scheduled and completed experiment total: {}.".format(num_exps))
        return num_exps

    def log_warped(self, logger):
        logger.info("Warped Pending_X:")
        logger.info(self.pending_X.warped)
        logger.info("Warped Finished_X:")
        logger.info(self.finished_X.warped)
        logger.info("Finished_Y:")
        logger.info(self.finished_Y)
        logger.info("Finished_Y_var:")
        logger.info(self.finished_Y_var)

    def log_unwarped(self, logger):
        logger.info("Unwarped Pending_X:")
        logger.info(self.pending_X.unwarped)
        logger.info("Unwarped Finished_X:")
        logger.info(self.finished_X.unwarped)
        logger.info("Finished_Y:")
        logger.info(self.finished_Y)
        logger.info("Finished_Y_var:")
        logger.info(self.finished_Y_var)

    @classmethod
    def _filter_exps(cls, exp_list):
        pending_exps = []  # Running exp but has not return any metric.
        running_exps = []  # Running exp and with partial metric.
        finished_exps = []  # Finished exp with final metric.

        for exp in exp_list:
            if exp.latestMetric and exp.stopped:
                finished_exps.append(exp)
                continue

            if exp.latestMetric and (not exp.stopped):
                running_exps.append(exp)
                continue

            if (not exp.latestMetric) and (not exp.stopped):
                pending_exps.append(exp)
                continue

        return pending_exps, running_exps, finished_exps

    @classmethod
    def _extract_hparamValue_from_exps(cls, hparamSettingList, pending_exps, running_exps,
                                       finished_exps):
        pending_X_list = [extract_hparamValue_single(hparamSettingList, exp)
                          for exp in chain(pending_exps, running_exps)]

        finished_X_list = [extract_hparamValue_single(hparamSettingList, exp)
                           for exp in finished_exps]

        if pending_X_list:
            pending_X = np.vstack(pending_X_list)
        else:
            pending_X = None

        if finished_X_list:
            finished_X = np.vstack(finished_X_list)
        else:
            finished_X = None

        # GPyOpt takes Y as (N, 1) matrix.
        finished_Y = np.array([
            extract_latestMetricValue_single(exp)[0] for exp in finished_exps
        ]).reshape((-1, 1))

        finished_Y_var = np.array([
            extract_latestMetricValue_single(exp)[1] for exp in finished_exps
        ]).reshape((-1, 1))

        if not finished_Y.any():
            finished_Y = None
            finished_Y_var = None

        # Package data into RegularXData obj to include warping info.
        warping_list = gen_warping_list(hparamSettingList)
        pending_X_obj = RegularXData.from_unwarped(pending_X, warping_list)
        finished_X_obj = RegularXData.from_unwarped(finished_X, warping_list)

        return pending_X_obj, finished_X_obj, finished_Y, finished_Y_var


class RegularXData(object):

    def __init__(self, X_warped, X_unwarped, warping_list):
        self.warping_list = warping_list
        self._X_warped = X_warped
        self._X_unwarped = X_unwarped

    @property
    def warped(self):
        return self._X_warped

    @property
    def unwarped(self):
        return self._X_unwarped

    @property
    def shape(self):
        assert self._X_warped.shape == self._X_unwarped.shape
        return self._X_warped.shape

    @property
    def empty(self):
        warped_empty = self._X_warped is None
        unwarped_empty = self._X_unwarped is None
        assert warped_empty == unwarped_empty
        return warped_empty

    @classmethod
    def from_unwarped(cls, X_unwarped, warping_list):
        X_warped = apply_warping_list(X_unwarped, warping_list, inplace=False)
        obj = cls(X_warped, X_unwarped, warping_list)
        return obj

    @classmethod
    def from_warped(cls, X_warped, warping_list):
        X_unwarped = apply_warping_list(X_warped, warping_list, inplace=False, inverse=True)
        obj = cls(X_warped, X_unwarped, warping_list)
        return obj


def apply_warping_list(X, warping_list, inplace=False, inverse=False):
    if X is None:
        return None
    assert X.shape[1] == len(warping_list)

    if inplace:
        warped = X
    else:
        warped = X.copy()

    for dimension in range(X.shape[1]):
        warping = warping_list[dimension]
        if inverse:
            warped[:, dimension] = warping.inverse(warped[:, dimension])
        else:
            warped[:, dimension] = warping.transform(warped[:, dimension])
    return warped


def extract_hparamValue_to_dict(exp):
    valueList = exp.hparamValue
    valueDict = {hparam.name: hparam.value for hparam in valueList}
    return valueDict


def extract_hparamValue_single(hparamSettingList, exp):
    X_list = []
    valueDict = extract_hparamValue_to_dict(exp)

    for hparamSetting in hparamSettingList:
        X_list.append(valueDict[hparamSetting.name])

    return np.array(X_list)


def extract_latestMetricValue_single(exp):
    # For compatibiltiy of schema change
    value_var = getattr(exp.latestMetric, 'value_var', 0.)
    return exp.latestMetric.value, value_var


def gen_warping_list(hparamSettingList):
    return [gen_warping_single(hparam) for hparam in hparamSettingList]


def gen_warping_single(hparamSetting):
    assert hparamSetting.type == FLOAT
    assert hparamSetting.warping == LINEAR
    return LinearWarping(hparamSetting.valrange.lower,
                         hparamSetting.valrange.upper)
