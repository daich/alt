import numpy as np


class Warping(object):
    def __init__(self):
        pass

    def transform(self):
        raise NotImplementedError

    def inverse(self):
        raise NotImplementedError


class LinearWarping(Warping):

    def __init__(self, dom_lower, dom_upper, img_lower=0., img_upper=10.):
        # There is a bug in GPyOpt s.t. if img is [0, 1] then there will be instability.
        # We use img range [0, 10] instead.
        assert dom_upper > dom_lower
        assert img_upper > img_lower

        self.dom_upper = dom_upper
        self.dom_lower = dom_lower
        self.img_upper = img_upper
        self.img_lower = img_lower

        self.dom_diff = self.dom_upper - self.dom_lower
        self.img_diff = self.img_upper - self.img_lower
        self.cross = self.dom_lower * self.img_upper - self.img_lower * self.dom_upper

    def transform(self, X, segment=False):
        y = (-X * self.img_diff + self.cross) / -self.dom_diff

        # Segment length does not have the constraint of domain
        if not segment:
            assert np.all(self.img_lower <= y) and np.all(y <= self.img_upper)
        return y

    def statement_transform(self, statement):
        # Transform a executable statement in which the old_substring is replaced by the
        # transformed substring
        new_statement = "((-({old}) * {img_diff} + {cross}) / -{dom_diff})".format(
            old=statement,
            img_diff=self.img_diff,
            cross=self.cross,
            dom_diff=self.dom_diff,
        )
        return new_statement

    def inverse(self, y, segment=False):
        X = (-y * self.dom_diff - self.cross) / -self.img_diff

        # Segment length does not have the constraint of domain
        if not segment:
            assert np.all(self.dom_lower <= X) and np.all(X <= self.dom_upper)
        return X

    def statement_inverse(self, statement):
        # InvTransform a executable statement in which the old_substring is replaced by the
        # transformed substring
        new_statement = "((-({old}) * {dom_diff} - {cross}) / -{img_diff})".format(
            old=statement,
            dom_diff=self.dom_diff,
            cross=self.cross,
            img_diff=self.img_diff,
        )
        return new_statement


class IdentityWarping(Warping):

    def __init__(self):
        pass

    def transform(self, X):
        return X

    def statement_transform(self, statement, old_substring):
        return statement

    def inverse(self, y):
        return y
