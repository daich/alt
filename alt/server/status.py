from alt.proto.python_build import status_pb2

SUCCESS = status_pb2.Status.Value('SUCCESS')
PENDING = status_pb2.Status.Value('PENDING')
FAIL = status_pb2.Status.Value('FAIL')

SUCCESS_STATS_RESP = status_pb2.StatusResponse(status=1)
