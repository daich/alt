import datetime
import munch
from protobuf_to_dict import dict_to_protobuf
from alt.common.protobuf_to_dict import safe_protobuf_to_dict
from pymongo import MongoClient
import logging

from alt.server.interface.interface_base import (
    ServerInterfaceBase, ServerInternalBase, safe_obj_id)
from alt.proto.python_build import (
    service_pb2,
    study_pb2,
    experiment_pb2,
    status_pb2,
    metric_pb2,
)
from alt.server.core.external_exception import (
    StudyAlreadyExistError, StudyNotFoundError,
    ExperimentNotFoundError, ExperimentAlreadyFinishedError,
)
from alt.server.optimization.selector import backend_selector
from alt.server import data_validation
from alt.server.status import SUCCESS, SUCCESS_STATS_RESP

_logger = logging.getLogger("ALT Servicer Interface")
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)


def _fill_sucess_statusresp(response):
    response.statusResponse.status = SUCCESS
    response.statusResponse.msg = ''


# ---------- Study ---------- #


class SvcRpcCreateStudy(ServerInterfaceBase):
    RPC_NAME = 'createStudy'
    PB_INPUT = study_pb2.StudySetting
    PB_RESPONSE = study_pb2.StudyID

    @staticmethod
    def method(servicer, request, context):
        collection_name = 'study'
        studySetting = request

        studyName = studySetting.studyName
        data_validation.hparam_setting_validate(studySetting.hparamSetting)
        for constraint_item in studySetting.constraints:
            data_validation.constraint_str_validate(constraint_item.constraint)

        if servicer._exist_study_by_name(studyName):
            raise StudyAlreadyExistError

        # Conversion of studySetting pb2 to dict, the constraints are converted separately to avoid
        # a pb2 issue of not having default value for composite repeated fields.
        pb2_constraints = studySetting.constraints
        dict_constraints = [safe_protobuf_to_dict(cons) for cons in pb2_constraints]
        del studySetting.constraints[:]
        studySetting.constraints.extend([study_pb2.Constraint()])
        study_dict = safe_protobuf_to_dict(studySetting)
        study_dict['constraints'] = dict_constraints

        study_dict['timeCreated'] = datetime.datetime.utcnow()
        study_dict['stopped'] = False

        with MongoClient(servicer.db.ATLAS_ADDR) as client:
            collection = servicer.db.get_collection(client, collection_name)
            study_id = collection.insert_one(study_dict).inserted_id

        resp = study_pb2.StudyID(studyID=str(study_id))
        return resp


class SvcRpcInfoStudyToDict(ServerInternalBase):
    "Internal convenience API, not exposed to client."

    RPC_NAME = 'infoStudy_todict'

    @staticmethod
    def method(servicer, request, context):
        collection_name = 'study'
        study_id = request.studyID

        with MongoClient(servicer.db.ATLAS_ADDR) as client:
            collection = servicer.db.get_collection(client, collection_name)
            study_dict = collection.find_one({"_id": safe_obj_id(study_id)})

        if study_dict is None:
            raise StudyNotFoundError
        study_dict['studyID'] = str(study_dict.pop('_id'))
        return study_dict


class SvcRpcInfoStudy(ServerInterfaceBase):
    RPC_NAME = 'infoStudy'
    PB_INPUT = study_pb2.StudyID
    PB_RESPONSE = status_pb2.StatusResponse

    @staticmethod
    def method(servicer, request, context):
        _logger.info('Enter info Study')
        collection_name = 'study'
        study_id = request.studyID

        with MongoClient(servicer.db.ATLAS_ADDR) as client:
            collection = servicer.db.get_collection(client, collection_name)
            study_dict = collection.find_one({"_id": safe_obj_id(study_id)})

        if study_dict is None:
            raise StudyNotFoundError

        study_dict['studyID'] = str(study_dict.pop('_id'))
        response = dict_to_protobuf(study_pb2.StudySetting, values=study_dict)
        _fill_sucess_statusresp(response)
        return response


class SvcRpcShouldStopStudy(ServerInterfaceBase):
    RPC_NAME = 'shouldStopStudy'
    PB_INPUT = study_pb2.StudyID
    PB_RESPONSE = service_pb2.SimpleBool

    @staticmethod
    def method(servicer, request, context):
        collection_name = 'study'
        study_id = request.studyID

        if not servicer._exist_study_by_id(study_id):
            raise StudyNotFoundError

        with MongoClient(servicer.db.ATLAS_ADDR) as client:
            collection = servicer.db.get_collection(client, collection_name)
            collection.update_one({"_id": safe_obj_id(study_id)}, {
                '$set': {
                    'timeStopped': datetime.datetime.utcnow(),
                    'stopped': True,
                }
            })
        return SUCCESS_STATS_RESP


# ---------- Experiment ---------- #


class SvcRpcCreateExperiment(ServerInterfaceBase):
    RPC_NAME = 'createExperiment'
    PB_INPUT = study_pb2.StudyID
    PB_RESPONSE = experiment_pb2.ExperimentID

    @staticmethod
    def method(servicer, request, context):
        study_id = request.studyID

        if not servicer._exist_study_by_id(study_id):
            raise StudyNotFoundError

        with MongoClient(servicer.db.ATLAS_ADDR) as client:
            if not servicer._exist_study_by_id(study_id):
                raise StudyNotFoundError

            exps = servicer.db.get_all_exp_in_study(client, study_id)
            exps = list(map(munch.munchify, exps))

            studySetting = servicer.infoStudy(request, context)

            opt_backend = backend_selector(studySetting)
            hparamValue = opt_backend.getNextExp(studySetting, exps)

            collection = servicer.db.get_exp_collection(client, study_id)
            exp_dict = {
                'studyID': study_id,
                'hparamValue': hparamValue,
                'timeCreated': datetime.datetime.utcnow(),
                'latestMetric': {},
                'fullMetricHistory': [],
                'stopped': False,
            }
            serialized_plot = opt_backend.plot_and_serialize(studySetting, exps)
            if serialized_plot is not None:
                exp_dict['plot'] = serialized_plot

            exp_id = collection.insert_one(exp_dict).inserted_id

        resp = experiment_pb2.ExperimentID(
            studyID=study_id, experimentID=str(exp_id))
        return resp


class SvcRpcCreateExperimentWithHparam(ServerInterfaceBase):
    """ RPC for creating experiment with predetermined Hparam values.
    """
    RPC_NAME = 'createExperimentWithHparam'
    PB_INPUT = experiment_pb2.ExperimentWithHparam
    PB_RESPONSE = experiment_pb2.ExperimentID

    @staticmethod
    def method(servicer, request, context):
        expSetting = request
        study_id = request.studyID

        if not servicer._exist_study_by_id(study_id):
            raise StudyNotFoundError

        with MongoClient(servicer.db.ATLAS_ADDR) as client:
            if not servicer._exist_study_by_id(study_id):
                raise StudyNotFoundError
            studySetting = servicer.infoStudy(request, context)

            data_validation.hparams_validate(studySetting.hparamSetting,
                                             expSetting.hparamValue)
            hparam_value = safe_protobuf_to_dict(expSetting)['hparamValue']

            collection = servicer.db.get_exp_collection(client, study_id)
            exp_dict = {
                'studyID': study_id,
                'hparamValue': hparam_value,
                'timeCreated': datetime.datetime.utcnow(),
                'latestMetric': {},
                'fullMetricHistory': [],
                'stopped': False,
            }
            exp_id = collection.insert_one(exp_dict).inserted_id

        resp = experiment_pb2.ExperimentID(
            studyID=study_id, experimentID=str(exp_id))
        return resp


class SvcRpcInfoExperiment(ServerInterfaceBase):
    RPC_NAME = 'infoExperiment'
    PB_INPUT = experiment_pb2.ExperimentID
    PB_RESPONSE = experiment_pb2.ExperimentSetting

    @staticmethod
    def method(servicer, request, context):
        study_id = request.studyID
        exp_id = request.experimentID

        with MongoClient(servicer.db.ATLAS_ADDR) as client:
            if not servicer._exist_study_by_id(study_id):
                raise StudyNotFoundError
            collection = servicer.db.get_exp_collection(client, study_id)
            exp_dict = collection.find_one({"_id": safe_obj_id(exp_id)})

        if exp_dict is None:
            raise ExperimentNotFoundError

        exp_dict['experimentID'] = str(exp_dict.pop('_id'))
        if 'plot' in exp_dict:
            del exp_dict['plot']
        response = dict_to_protobuf(
            experiment_pb2.ExperimentSetting, values=exp_dict)
        _fill_sucess_statusresp(response)
        return response


class SvcRpcStopExperiment(ServerInterfaceBase):
    RPC_NAME = 'stopExperiment'
    PB_INPUT = experiment_pb2.ExperimentID
    PB_RESPONSE = status_pb2.StatusResponse

    @staticmethod
    def method(servicer, request, context):
        study_id = request.studyID
        exp_id = request.experimentID

        with MongoClient(servicer.db.ATLAS_ADDR) as client:
            if not servicer._exist_study_by_id(study_id):
                raise StudyNotFoundError
            collection = servicer.db.get_exp_collection(client, study_id)
            collection.update_one({"_id": safe_obj_id(exp_id)}, {
                '$set': {
                    'timeStopped': datetime.datetime.utcnow(),
                    'stopped': True,
                }
            })
        return SUCCESS_STATS_RESP


# ---------- Metric ---------- #


class SvcRpcUpdateMetric(ServerInterfaceBase):
    RPC_NAME = 'updateMetric'
    PB_INPUT = metric_pb2.ExpMetric
    PB_RESPONSE = status_pb2.StatusResponse

    @staticmethod
    def method(servicer, request, context):
        study_id = request.studyID
        exp_id = request.experimentID

        metric = request.metric
        metric.filled = True

        metric = safe_protobuf_to_dict(metric)
        metric['timeCreated'] = datetime.datetime.utcnow()

        with MongoClient(servicer.db.ATLAS_ADDR) as client:
            if not servicer._exist_study_by_id(study_id):
                raise StudyNotFoundError
            collection = servicer.db.get_exp_collection(client, study_id)

            exp = collection.find_one({"_id": safe_obj_id(exp_id)})
            if exp['stopped']:
                raise ExperimentAlreadyFinishedError

            # Update `latestMetric` and `fullMetricHistory` in one trasaction.
            collection.update_one(
                {"_id": safe_obj_id(exp_id)},
                {
                    '$set': {
                        'latestMetric': metric
                    },
                    '$push': {
                        'fullMetricHistory': metric
                    },
                },
                upsert=False,
            )
        return SUCCESS_STATS_RESP


class SvcRpcFinishExperimentWithMetric(ServerInterfaceBase):
    RPC_NAME = 'finishExperimentWithMetric'
    PB_INPUT = metric_pb2.ExpMetric
    PB_RESPONSE = status_pb2.StatusResponse

    @staticmethod
    def method(servicer, request, context):
        study_id = request.studyID
        exp_id = request.experimentID

        metric = request.metric
        metric.filled = True

        metric = safe_protobuf_to_dict(metric)
        metric['timeCreated'] = datetime.datetime.utcnow()

        with MongoClient(servicer.db.ATLAS_ADDR) as client:
            if not servicer._exist_study_by_id(study_id):
                raise StudyNotFoundError
            collection = servicer.db.get_exp_collection(client, study_id)

            exp = collection.find_one({"_id": safe_obj_id(exp_id)})
            if not exp:
                raise ExperimentNotFoundError
            if exp['stopped']:
                raise ExperimentAlreadyFinishedError

            # Update `latestMetric` and `fullMetricHistory` in one trasaction.
            collection.update_one(
                {"_id": safe_obj_id(exp_id)},
                {
                    '$set': {
                        'latestMetric': metric
                    },
                    '$push': {
                        'fullMetricHistory': metric
                    },
                },
                upsert=False,
            )
        return servicer.stopExperiment(request, context)
