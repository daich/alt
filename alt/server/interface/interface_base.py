import bson

from alt.server.core.external_exception import ExtException, IdNotValidError


class ServerInterfaceBase(object):
    '''
    This abstraction provides the rpc call abstraction for all server interface functions.
    This is needed for the common ExtException (exceptions that can be exposed to clients)
    handling mechanism.
    '''
    RPC_NAME = ""
    PB_INPUT = None
    PB_RESPONSE = None

    def __init__(self, db):
        self.db = db

    @staticmethod
    def method(servicer, request, context):
        raise NotImplementedError

    def gen_rpc(self):
        return self.servicer_extexception_handler(self.method)

    @classmethod
    def servicer_extexception_handler(cls, grpc_api):

        def wrapped(*args):
            if len(args) != 3:
                raise ValueError(args)
            servicer, request, context = args
            try:
                resp = grpc_api(servicer, request, context)
            except ExtException as extexc:
                context.set_code(extexc.grpc_status_code)
                context.set_details(extexc.msg)
                resp = cls.PB_RESPONSE()
            return resp

        return wrapped


class ServerInternalBase(ServerInterfaceBase):
    """ Helper class for adding convenience server-internal API to servicer.
        It will not generate client facing RPC.
    """

    def gen_rpc(self):
        return self.method


def safe_obj_id(some_id):
    try:
        val = bson.objectid.ObjectId(some_id)
    except bson.errors.InvalidId:
        raise IdNotValidError
    return val
