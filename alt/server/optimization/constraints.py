from alt.server.core.data import gen_warping_list
from alt.server.optimization.utils import gen_hparamName_to_idx_table


class ConstraintTranscriptor(object):
    ''' Example:
        input     = "-x['Pe'] -.5 + abs(x['Pf']) - np.sqrt(1-x['Pf']**2)"
        tranlated = "-x[:,1] -.5 + abs(x[:,0]) - np.sqrt(1-x[:,0]**2)"
    '''
    def __init__(self, hparamSetting):
        self.hparam_idx_table = gen_hparamName_to_idx_table(hparamSetting)
        self.warping_list = gen_warping_list(hparamSetting)
        self.replace_table = self._gen_replace_table(self.hparam_idx_table)

    def _gen_replace_table(self, hparam_idx_table):
        tmpl_input = "x['{}']"
        tmpl_output = "x[:,{}]"

        replace_table = dict()
        for hparam, idx in self.hparam_idx_table.items():
            unwarped_statement = tmpl_output.format(idx)
            warped_statement = self.warping_list[idx].statement_inverse(unwarped_statement)
            replace_table[tmpl_input.format(hparam)] = warped_statement

        return replace_table

    def transcribe(self, string):
        output = string
        for source, target in self.replace_table.items():
            output = output.replace(source, target)
        return output
