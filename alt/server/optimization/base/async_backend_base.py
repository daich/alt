import base64
import GPyOpt
import io
import logging
import matplotlib.pyplot as plt
from matplotlib import patches
import numpy as np

from alt.proto.python_build import study_pb2
from alt.server.core.data import RegularExpData, gen_warping_single
from alt.server.optimization import utils
from alt.server.optimization.base.backend_base import OptBackendBase
from alt.server.optimization.binaryT.gpmodel_heteroscedastic import GPHeteroscedasticModel
from alt.server.optimization.constraints import ConstraintTranscriptor
from alt.server.utils import is_called_from_test, pytest_opt_plot_to_gui


_logger = logging.getLogger("Async Backend Base")
logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)

MAXIMIZE = study_pb2.StudyGoal.Value('MAXIMIZE')
DEFAULT_RANDOM_SEED = 111
plt.rc('font', family='serif')


class AsyncBackendBase(OptBackendBase):
    INIT_DESIGN_NUMDATA = None  # To be override with an int by subclasses.
    INVERT_METRIC_FOR_MAXIMIZE = True

    # The number of rounds of method `updateModel` called that would to trigger a full optimization
    # restart for the GPModel. This MUST be set to `1` to guarantee determinism and eliminate the
    # side effects of model's previous X/Y values.
    OPTIMIZE_RESTARTS = 1

    def shouldStopStudy(self, studySetting, exps):
        """ returns bool """
        return NotImplementedError

    def _gen_opt_sys(self, studySetting, exp_data):
        return NotImplementedError

    def _gen_data_obj(self, studySetting, exps):
        self.exp_data = RegularExpData.from_exps(exps, studySetting)
        return self.exp_data

    def getNextExp(self, studySetting, exps):
        exp_data = self._gen_data_obj(studySetting, exps)

        if self.INVERT_METRIC_FOR_MAXIMIZE and studySetting.goal == MAXIMIZE:
            if self.exp_data.finished_Y is not None:
                self.exp_data.finished_Y *= -1

        self.setup(studySetting, exp_data)

        if self._in_initial_design_phase(studySetting, exp_data):
            _logger.info("Initial Design Phase.")
            result = self._getNextExp_initial_design(studySetting, exp_data)
            return result
        else:
            _logger.info("Active Design Phase.")
            result = self._getNextExp_bayesian_opt(studySetting, exp_data)

            if is_called_from_test() and pytest_opt_plot_to_gui() and \
               studySetting.goal not in (1, 2):
                self.opt_sys.plot_acquisition(exp_data)
            return result

    def plot_and_serialize(self, studySetting, exps, plot_args=None):
        exp_data = self._gen_data_obj(studySetting, exps)

        if (not self._in_initial_design_phase(studySetting, exp_data)) and \
           studySetting.goal not in (1, 2):
            buffer = io.BytesIO()
            self.opt_sys.plot_acquisition(exp_data, buffer=buffer, plot_args=plot_args)
            buffer.seek(0)
            return base64.b64encode(buffer.read())
        else:
            return None

    def setup(self, studySetting, exp_data):
        self.hparamSetting = studySetting.hparamSetting

        self.space, self.constraints = self._parse_hparam(self.hparamSetting,
                                                          studySetting.constraints)
        self.space = GPyOpt.core.task.space.Design_space(self.space, self.constraints)

        self.design = GPyOpt.experiment_design.RandomDesign(self.space)
        self.opt_sys = self._gen_opt_sys(studySetting, exp_data)

    def _getNextExp_initial_design(self, studySetting, exp_data, random_seed=DEFAULT_RANDOM_SEED):
        num_exps = exp_data.num_scheduled(logger=_logger)

        # Use the number of scheduled exps to dedup the random seed, otherwise it always picks
        # the same point.
        np.random.seed(random_seed + num_exps)
        suggested_X = self.design.get_samples(1)
        return utils.unwarp_pack_hparamValueList(suggested_X, self.hparamSetting)

    def _in_initial_design_phase(self, studySetting, exp_data):
        num_exps = exp_data.num_scheduled(logger=_logger)
        return (num_exps < self.INIT_DESIGN_NUMDATA) or (exp_data.finished_Y is None)

    def _parse_hparam(self, hparamSettingList, constraints):
        bounds = []

        # TODO(daich): Change to allow for other types of variable.
        # TODO(daich): Change to allow for variable warping.
        for hparamSetting in hparamSettingList:
            warping = gen_warping_single(hparamSetting)
            setting_dict = {
                'name': hparamSetting.name,
                'type': 'continuous',
                'domain': (warping.img_lower, warping.img_upper),
            }
            bounds.append(setting_dict)

        if not constraints:
            # Following GPyOpt's default keyword arg value.
            transcribed_constraints = None
        else:
            transcribed_constraints = []
            transcriptor = ConstraintTranscriptor(hparamSettingList)

            for constraint_obj in constraints:
                regular_constraint = transcriptor.transcribe(constraint_obj.constraint)
                transcribed_constraints.append(dict(name=constraint_obj.constraintName,
                                                    constraint=regular_constraint))
        return bounds, transcribed_constraints

    def _getNextExp_bayesian_opt(self, studySetting, exp_data, random_seed=DEFAULT_RANDOM_SEED):
        suggested_X = self.opt_sys.suggest_next_locations(pending_X=exp_data.pending_X.warped,
                                                          ignored_X=None,
                                                          Y_var=exp_data.finished_Y_var)
        suggested_X = suggested_X[0, :]
        return utils.unwarp_pack_hparamValueList(suggested_X, self.hparamSetting)


class AsyncModularBO(GPyOpt.methods.ModularBayesianOptimization):
    """ Only overriding one method to provide support for passing `AsyncBatchManager` with
        pending_X data into evaluator's `compute_batch` method with minimal impact to GPyOpt
        other logic.
    """

    def suggest_next_locations(self, context=None, pending_X=None, ignored_X=None,
                               Y_var=None, random_seed=DEFAULT_RANDOM_SEED):
        """
        Run a single optimization step and return the next locations to evaluate the objective.
        Number of suggested locations equals to batch_size.
        :param context: fixes specified variables to a particular context (values) for the
                        optimization run (default, None).
        :param pending_X: matrix of input configurations that are in a pending state (i.e.,
                          do not have an evaluation yet) (default, None).
        :param ignored_X: matrix of input configurations that the user black-lists, i.e.,
                          those configurations will not be suggested again (default, None).
        """
        self.model_parameters_iterations = None
        self.num_acquisitions = 0
        self.context = context

        np.random.seed(random_seed)
        self._update_model(Y_var, self.normalization_type)
        suggested_locations = self._compute_next_evaluations(pending_zipped_X=pending_X,
                                                             ignored_zipped_X=ignored_X)
        return suggested_locations

    def _update_model(self, Y_var=None, normalization_type='stats'):
        if self.num_acquisitions % self.model_update_interval == 0:

            # input that goes into the model (is unziped in case there are categorical variables)
            X_inmodel = self.space.unzip_inputs(self.X)

            # Y_inmodel is the output that goes into the model
            if self.normalize_Y:
                Y_inmodel = GPyOpt.util.general.normalize(self.Y, normalization_type)
            else:
                Y_inmodel = self.Y

            # Provide the variance of Y to the model if needed (when the model is
            # heteroscedastic)
            if isinstance(self.model, GPHeteroscedasticModel):
                self.model.updateModel(X_inmodel, Y_inmodel, None, None, Y_var)
            else:
                self.model.updateModel(X_inmodel, Y_inmodel, None, None)

        # Save parameters of the model
        self._save_model_parameter_values()

    def _compute_next_evaluations(self,
                                  pending_zipped_X=None,
                                  ignored_zipped_X=None):
        """
        Computes the location of new evaluation, optimizes the acquisition in the standard case.
        :param pending_zipped_X: matrix of input configurations that are in a pending state,
                                 i.e., do not have an evaluation yet.
        :param ignored_zipped_X: matrix of input configurations that the user black-lists,
                                 i.e., those configurations will not be suggested again.
        :return:
        """

        # Use AsyncBatchManager at all time to manage possible pending_X.
        batch_manager = AsyncBatchManager(
            space=self.space,
            zipped_X=self.X,
            pending_zipped_X=pending_zipped_X,
            ignored_zipped_X=ignored_zipped_X)

        # We zip the value in case there are categorical variables.
        return self.space.zip_inputs(
            self.evaluator.compute_batch(duplicate_manager=batch_manager))

    def plot_acquisition(self, exp_data, buffer=None, plot_args=None):
        """
        Plots the model and the acquisition function, but with augmented functionalities
        to deal with pending experiments:
            1. Plot pending_X as scatter inside all subfigs, and add circle to demonstrate
               penalizing radius.
            2. Also add a subfig showing acquisition after penalization.
        """
        if self.model.model is None:
            raise RuntimeError("The plotting tool must be used after model updates.")

        model_to_plot = self.model
        suggested_X = self.suggest_next_locations(pending_X=exp_data.pending_X.warped,
                                                  ignored_X=None,
                                                  Y_var=exp_data.finished_Y_var)
        _logger.info('Plotting Suggested_X:')
        _logger.info(suggested_X)
        _logger.info('Plotting Pending_X:')
        _logger.info(exp_data.pending_X.warped)

        return self.plot_acquisition_2d_async(
            self.acquisition.space.get_bounds(),
            model_to_plot.model.X.shape[1], model_to_plot.model, exp_data,
            self.acquisition, suggested_X, buffer, plot_args=plot_args)

    def plot_acquisition_2d_async(self,
                                  bounds,
                                  input_dim,
                                  model,
                                  exp_data,
                                  acquisition,
                                  suggested_sample,
                                  filename=None,
                                  plot_args=None,
                                  grid_resolution=(200, 200)):
        if input_dim != 2:
            return

        warping_list = exp_data.warping_list
        unwarped_suggested_sample = utils.apply_warping_list(suggested_sample, warping_list,
                                                             inverse=True)
        X1 = np.linspace(bounds[0][0], bounds[0][1], grid_resolution[0])
        X2 = np.linspace(bounds[1][0], bounds[1][1], grid_resolution[1])
        num_mesh_points = grid_resolution[0] * grid_resolution[1]
        x1, x2 = np.meshgrid(X1, X2)
        X = np.hstack((x1.reshape(num_mesh_points, 1), x2.reshape(num_mesh_points, 1)))

        unwarped_X1 = np.linspace(warping_list[0].dom_lower, warping_list[0].dom_upper,
                                  grid_resolution[0])
        unwarped_X2 = np.linspace(warping_list[1].dom_lower, warping_list[1].dom_upper,
                                  grid_resolution[1])

        lipschitz_cnst = self.acquisition.estimate_L(model, bounds)
        Min = self.acquisition.model.model.Y.min()
        penalizing_tuple = \
            acquisition._hammer_function_precompute(exp_data.pending_X.warped,
                                                    lipschitz_cnst,
                                                    Min,
                                                    model)

        # Unpack the penalizing_tuple.
        if len(penalizing_tuple) == 4:
            penalizing_radius, penalizing_sigma = penalizing_tuple[:2]
            penalizing_radius_std, penalizing_sigma_std = penalizing_tuple[2:]
        elif len(penalizing_tuple) == 2:
            penalizing_radius, penalizing_sigma = penalizing_tuple
            penalizing_radius_std, penalizing_sigma_std = penalizing_tuple
        else:
            raise ValueError("The penalizing tuple should be length 4 or 2.")
        if penalizing_radius is None or penalizing_radius_std is None:
            penalizing_radius = 1e-13
        else:
            penalizing_radius = np.maximum(penalizing_radius, penalizing_radius_std)

        # Unpenalized acquisition value are obtained by setting pendingX to be None.
        acqu_penalized = acquisition._penalized_acquisition(
            X, model, exp_data.pending_X.warped,
            penalizing_radius, penalizing_sigma,
            penalizing_radius_std, penalizing_sigma_std)
        acqu = acquisition._penalized_acquisition(X, model, None, None, None, None, None)

        acqu_normalized = (-acqu - min(-acqu)) / (max(-acqu - min(-acqu)))
        acqu_normalized = acqu_normalized.reshape(grid_resolution)
        acqu_penalized_normalized = (
            -acqu_penalized - min(-acqu_penalized)) / (
                max(-acqu_penalized - min(-acqu_penalized)))
        acqu_penalized_normalized = acqu_penalized_normalized.reshape(grid_resolution)

        acqu = acqu.reshape(grid_resolution)
        acqu_penalized = acqu_penalized.reshape(grid_resolution)

        mean, var = model.predict(X)

        # Grid object generation.
        unwarped_bound_X1 = (warping_list[0].dom_lower, warping_list[0].dom_upper)
        unwarped_bound_X2 = (warping_list[1].dom_lower, warping_list[1].dom_upper)
        grid_obj = Grid2D(unwarped_X1, unwarped_X2, unwarped_bound_X1, unwarped_bound_X2)

        # Viz points generation.
        phase_0 = np.where(exp_data.finished_Y > 0)[0]
        phase_1 = np.where(exp_data.finished_Y <= 0)[0]
        finished_points_phase_0 = VizPoints2D(exp_data.finished_X.unwarped[phase_0, :],
                                              'r.', 'Observed Exps')
        finished_points_phase_1 = VizPoints2D(exp_data.finished_X.unwarped[phase_1, :],
                                              'r^', 'Observed Exps')
        pending_points = VizPoints2D(exp_data.pending_X.unwarped, 'r*', 'Pending Exps')
        suggested_points = VizPoints2D(unwarped_suggested_sample, 'k.', 'Suggested Exp')
        plt.figure(figsize=(15, 15))

        # ----- Mean
        subplot_tuple = (2, 2, 1)
        self._subplot_field("Posterior Mean",
                            grid_obj,
                            mean,
                            [finished_points_phase_0, finished_points_phase_1, pending_points],
                            subplot_tuple,
                            plot_args)

        # ----- Std
        subplot_tuple = (2, 2, 2)
        self._subplot_field("Posterior Std.",
                            grid_obj,
                            np.sqrt(var),
                            [finished_points_phase_0, finished_points_phase_1, pending_points],
                            subplot_tuple,
                            plot_args)

        # ----- Raw Acquisition
        subplot_tuple = (2, 2, 3)
        self._subplot_field("Raw Acquisition",
                            grid_obj,
                            -acqu,
                            [pending_points, suggested_points],
                            subplot_tuple,
                            plot_args)

        # Adding penalizing circles around the pending points.
        if not exp_data.pending_X.empty:
            for i in range(exp_data.pending_X.shape[0]):
                point = exp_data.pending_X.unwarped[i, :]
                radius = penalizing_radius[i]
                ellipse_r0 = exp_data.warping_list[0].inverse(radius, segment=True)
                ellipse_r1 = exp_data.warping_list[1].inverse(radius, segment=True)
                ellipse = patches.Ellipse(point, ellipse_r0 * 2, ellipse_r1 * 2,
                                          edgecolor='k', facecolor='k', alpha=0.2)
                plt.gca().add_artist(ellipse)

        # ----- Penalized Acquisition
        subplot_tuple = (2, 2, 4)
        self._subplot_field("Penalized Acquisition",
                            grid_obj,
                            -acqu_penalized,
                            [pending_points, suggested_points],
                            subplot_tuple,
                            plot_args)

        if filename is not None:
            plt.savefig(filename)
        else:
            plt.show()

    def _subplot_field(self, title, grid_obj, field, viz_points_list, subplot_tuple,
                       plot_args=None):
        plt.subplot(*subplot_tuple)
        mean_data_block = VizDataBlock2D(title, grid_obj, field, viz_points_list, plot_args)
        mean_data_block.plot_all()


class AsyncBatchManager(object):

    def __init__(self,
                 space,
                 zipped_X,
                 pending_zipped_X=None,
                 ignored_zipped_X=None):
        self.finished_X = zipped_X
        self.pending_X = pending_zipped_X


class VizDataBlock2D(object):

    def __init__(self, title, grid_obj, field, viz_points_list, plot_args=None):
        self.title = title

        self.grid_obj = grid_obj
        self.viz_points_list = viz_points_list
        self.field = field.reshape(self.grid_obj.resolution())
        self.plot_args = plot_args

        if plot_args and 'X1_name' in plot_args:
            self.X1_name = plot_args['X1_name']
            self.X2_name = plot_args['X2_name']
        else:
            self.X1_name = 'X1'
            self.X2_name = 'X2'

    def plot_all(self):
        self.contourf()
        self.plot_points()
        self.plot_axes()

        if self.plot_args and 'xlim' in self.plot_args:
            plt.xlim(self.plot_args['xlim'])
        if self.plot_args and 'ylim' in self.plot_args:
            plt.ylim(self.plot_args['ylim'])

    def contourf(self, contour_levels=100):
        plt.contourf(self.grid_obj.grid_X1,
                     self.grid_obj.grid_X2,
                     self.field,
                     contour_levels)
        plt.title(self.title, fontsize=20)
        colorbar = plt.colorbar()
        colorbar.ax.tick_params(labelsize=13)
        self.grid_obj.gen_plt_axis()

    def plot_axes(self):
        ax = plt.gca()
        plt.xlabel(self.X1_name, fontsize=18)
        plt.ylabel(self.X2_name, fontsize=18)
        ax.tick_params(axis='both', which='major', labelsize=13)

    def plot_points(self, markersize=10):
        for points_type in self.viz_points_list:
            points_type.plot()


class Grid2D(object):

    def __init__(self, grid_X1, grid_X2, bounds_X1, bounds_X2,):
        self.grid_X1 = grid_X1
        self.grid_X2 = grid_X2
        self.bounds_X1 = bounds_X1
        self.bounds_X2 = bounds_X2
        self.grid_resolution = (len(self.grid_X1), len(self.grid_X2))

        assert np.all(bounds_X1[0] <= grid_X1) and np.all(grid_X1 <= bounds_X1[1])
        assert np.all(bounds_X2[0] <= grid_X2) and np.all(grid_X2 <= bounds_X2[1])

    def gen_plt_axis(self):
        plt.axis((*self.bounds_X1, *self.bounds_X2))

    def resolution(self):
        return self.grid_resolution


class VizPoints2D(object):

    def __init__(self, points, style, label):
        self.points = points
        self.style = style
        self.label = label

        if self.points is not None:
            assert self.points.shape[1] == 2

    def plot(self, markersize=10):
        if self.points is None:
            return
        plt.plot(self.points[:, 0], self.points[:, 1], self.style,
                 markersize=markersize, label=self.label)
