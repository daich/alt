# Modified from GPyOpt.acquisition.AcquisitionLP
# https://github.com/SheffieldML/GPyOpt/blob/master/GPyOpt/acquisitions/LP.py

from GPyOpt.util.general import samples_multidimensional_uniform
import logging
import numpy as np
import scipy
from scipy.stats import norm


from alt.server.optimization.binaryT.acquisition_binaryT_lp import AcquisitionBinaryTLP


_logger = logging.getLogger("BinaryT Local Penalization Acquisition")
logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)


class AcquisitionLcbLP(AcquisitionBinaryTLP):
    analytical_gradient_prediction = True

    def _hammer_function_precompute(self, x0, L, Min, model):
        """
        Pre-computes the parameters of a penalizer centered at x0, here x0
        can be either a vector (single x0) or a 2d np array (multiple x0).
        """
        if x0 is None:
            return None, None
        if len(x0.shape) == 1:
            x0 = x0[None, :]
        m = model.predict(x0)[0]
        pred = model.predict(x0)[1].copy()
        pred[pred < 1e-16] = 1e-16
        s = np.sqrt(pred)

        # ---------- GPyOpt original code
        r_x0 = np.abs(m - Min) / L
        s_x0 = (np.abs(s)) / L
        # ----------

        r_x0 = r_x0.flatten()
        s_x0 = s_x0.flatten()

        _logger.info("----- Entering Estimation for Penalizing Parameters -----")
        _logger.info("Pending Experiments:")
        _logger.info(x0)
        _logger.info("Penalizing Radius for pending exps:")
        _logger.info(r_x0)
        _logger.info("Penalizing Sigma for pending exps:")
        _logger.info(s_x0)

        return r_x0, s_x0

    @classmethod
    def estimate_L(cls, model, bounds, storehistory=True):
        """
        Estimate the Lipschitz constant of f by taking maximizing the norm of the expectation
        of the gradient of *f*.
        """

        def df(x, model, x0):
            x = np.atleast_2d(x)
            dmdx, dvdx = model.predictive_gradients(x)
            dmdx = np.squeeze(dmdx, axis=2)

            # ---------- Original code version in GPyOpt
            # simply take the norm of the expectation of the gradient
            res = np.sqrt((dmdx * dmdx).sum(1))
            # ----------

            res = res.reshape((-1, 1))
            return -res

        samples = samples_multidimensional_uniform(bounds, 500)
        samples = np.vstack([samples, model.X])
        pred_samples = df(samples, model, 0)
        x0 = samples[np.argmin(pred_samples)]
        res = scipy.optimize.minimize(
            df,
            x0,
            method='L-BFGS-B',
            bounds=bounds,
            args=(model, x0),
            options={'maxiter': 200},
        )
        minusL = res.fun[0][0]
        L = -minusL
        if L < 1e-7:
            L = 10  # To avoid problems in cases in which the model is flat.
        return L

    def update_batches(self, X_batch, L, Min):
        """
        Updates the batches internally and pre-computes the hammer function for penalization.
        """
        self.X_batch = X_batch
        if X_batch is not None:
            self.r_x0_mean, self.s_x0_mean = \
                self._hammer_function_precompute(
                    X_batch,
                    L,
                    Min,
                    self.model)
        else:
            self.r_x0_mean = None
            self.s_x0_mean = None

    def _penalized_acquisition(self, x, model, X_batch,
                               r_x0_mean, s_x0_mean,
                               r_x0_std, s_x0_std):
        '''
        Creates a penalized acquisition function using 'hammer' functions around the points
        collected in the batch.

        .. Note:: the penalized acquisition is always mapped to the log space. This way gradients
                  can be computed additively and are more stable.
        '''
        fval = -self.acq.acquisition_function(x)[:, 0]

        if self.transform == 'softplus':
            fval_org = fval.copy()
            fval[fval_org >= 40.] = np.log(fval_org[fval_org >= 40.])
            fval[fval_org < 40.] = np.log(
                np.log1p(np.exp(fval_org[fval_org < 40.])))
        elif self.transform == 'none':
            fval = np.log(fval + 1e-50)

        fval = -fval
        if X_batch is not None:
            h_vals = self._hammer_function(x, X_batch, r_x0_mean, s_x0_mean)
            fval += -h_vals.sum(axis=-1)
        return fval

    def _d_hammer_function(self, x, X_batch, r_x0, s_x0):
        """
        Computes the gradient of the log penalizer (centered at x_0) at any x.
        """
        dx = np.atleast_2d(x)[:, None, :] - np.atleast_2d(X_batch)[None, :, :]
        nm = np.sqrt((np.square(dx)).sum(-1))
        z = (nm - r_x0) / s_x0
        h_func = norm.cdf(z)

        d = 1. / (s_x0 * np.sqrt(2 * np.pi) * h_func) * np.exp(
            -np.square(z) / 2) / nm
        d[h_func < 1e-50] = 0.
        # original wrong code
        # d = d[:,:,None]
        # should be changed to:
        d = d[:, :, None]

        # Before this line, d : (dim. of x, dim. of X_batch, 1)
        #                   dx: (dim. of x, dim. of X_batch, dim. of parameter space).
        d = d * dx

        # Sum over all X_batch elements.
        d = d.sum(axis=1)
        return d

    def d_acquisition_function(self, x):
        """
        Returns the gradient of the acquisition function at x.
        """
        x = np.atleast_2d(x)

        if self.transform == 'softplus':
            fval = -self.acq.acquisition_function(x)[:, 0]
            scale = 1. / (np.log1p(np.exp(fval)) * (1. + np.exp(-fval)))
        elif self.transform == 'none':
            fval = -self.acq.acquisition_function(x)[:, 0]
            scale = 1. / fval
        else:
            scale = 1.

        if self.X_batch is None:
            _, grad_acq_x = self.acq.acquisition_function_withGradients(x)
            return scale * grad_acq_x
        else:
            _, grad_acq_x = self.acq.acquisition_function_withGradients(x)
            return scale * grad_acq_x \
                - self._d_hammer_function(x, self.X_batch, self.r_x0_mean, self.s_x0_mean)
