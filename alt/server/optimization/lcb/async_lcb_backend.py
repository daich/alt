import GPyOpt
from GPyOpt.acquisitions import AcquisitionLCB
from GPyOpt.optimization.acquisition_optimizer import AcquisitionOptimizer
import logging

from alt.server.optimization.base.async_backend_base import AsyncBackendBase, AsyncModularBO
from alt.server.optimization.lcb.acquisition_lcb_lp import AcquisitionLcbLP
from alt.server.optimization.binaryT.evaluator_async_lp import AsyncLocalPenalization


_logger = logging.getLogger("Async LCB Backend")
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)


class AsyncLcbBackend(AsyncBackendBase):
    INIT_DESIGN_NUMDATA = 5

    def shouldStopStudy(self, studySetting, exps):
        """ returns bool """
        return NotImplementedError

    def _gen_opt_sys(self, studySetting, exp_data):
        model = GPyOpt.models.GPModel(optimize_restarts=self.OPTIMIZE_RESTARTS, verbose=False)
        acquisition_optimizer = AcquisitionOptimizer(self.space)

        lcb_acquisition = AcquisitionLCB(model, self.space,
                                         acquisition_optimizer)
        lp_acquisition = AcquisitionLcbLP(
            model, self.space, acquisition_optimizer, lcb_acquisition)
        self.acquisition = lp_acquisition

        evaluator = AsyncLocalPenalization(lp_acquisition)
        objective = None

        opt_sys = AsyncModularBO(model, self.space, objective, lp_acquisition,
                                 evaluator, self.design, normalize_Y=False)
        opt_sys.X = exp_data.finished_X.warped
        opt_sys.Y = exp_data.finished_Y
        return opt_sys
