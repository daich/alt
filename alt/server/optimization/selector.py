from alt.proto.python_build import study_pb2
from alt.server.optimization.lcb.async_lcb_backend import AsyncLcbBackend
from alt.server.optimization.binaryT.async_binaryT_backend import (
    AsyncBinaryTBackend, AsyncBinaryTHeteroscedasticBackend)
from alt.server.optimization.binaryT.async_pure_exploration_backend import (
    AsyncPureExplorationBackend, AsyncPureExplorationHeteroscedasticBackend)

MAXIMIZE = study_pb2.StudyGoal.Value('MAXIMIZE')
MINIMIZE = study_pb2.StudyGoal.Value('MINIMIZE')
BINARYT = study_pb2.StudyGoal.Value('BINARYT')
BINARYT_REPLICAS = study_pb2.StudyGoal.Value('BINARYT_REPLICAS')
PURE_EXPLORATION = study_pb2.StudyGoal.Value('PURE_EXPLORATION')
PURE_EXPLORATION_REPLICAS = study_pb2.StudyGoal.Value('PURE_EXPLORATION_REPLICAS')


OPT_BACKEND_MAP = {
    MAXIMIZE: AsyncLcbBackend,
    MINIMIZE: AsyncLcbBackend,
    BINARYT: AsyncBinaryTBackend,
    BINARYT_REPLICAS: AsyncBinaryTHeteroscedasticBackend,
    PURE_EXPLORATION: AsyncPureExplorationBackend,
    PURE_EXPLORATION_REPLICAS: AsyncPureExplorationHeteroscedasticBackend,
}


def backend_selector(studySetting):
    goal = studySetting.goal
    # TODO: should unify minimize and maximize by passing different values into ctors.
    return OPT_BACKEND_MAP[goal]()
