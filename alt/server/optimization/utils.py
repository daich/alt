import logging

from alt.server.core.data import apply_warping_list, gen_warping_list


_logger = logging.getLogger("Opt utils")
logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)


def unwarp_pack_hparamValueList(X, hparamSettingList):
    warping_list = gen_warping_list(hparamSettingList)
    unwarped_X = apply_warping_list(X.reshape((1, -1)),
                                    warping_list,
                                    inverse=True)
    return _pack_hparamValueList(unwarped_X, hparamSettingList)


def _pack_hparamValueList(X, hparamSettingList):
    """ [Critical]: The ordering of X and hparamSettingList are meaningful and preserved in both
        server and clients. The information is used to pack and unpack hparamValueList objects.
    """
    X = X.ravel()

    if len(X) != len(hparamSettingList):
        raise ValueError(
            'Packing error when packing hparam values, len is not the same in setting and values.'
        )

    hparamValueList = []
    for hparamSetting, value in zip(hparamSettingList, X):
        hparamValueList.append({
            'name': hparamSetting.name,
            'value': value,
            'type': hparamSetting.type,
        })
    return hparamValueList


def gen_hparamName_to_idx_table(hparamSettingList):
    ret = dict()
    for idx, hparamSetting in enumerate(hparamSettingList):
        ret[hparamSetting.name] = idx
    return ret
