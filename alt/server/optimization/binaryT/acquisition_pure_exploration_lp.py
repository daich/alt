# Modified from GPyOpt.acquisition.AcquisitionLP
# https://github.com/SheffieldML/GPyOpt/blob/master/GPyOpt/acquisitions/LP.py

from GPyOpt.acquisitions import AcquisitionLCB, AcquisitionLCB_MCMC
import logging
import numpy as np

from alt.server.optimization.binaryT.acquisition_binaryT_lp import AcquisitionBinaryTLP

_logger = logging.getLogger("BinaryT Local Penalization Acquisition")
logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)


class AcquisitionPureExplorationLP(AcquisitionBinaryTLP):
    """
    Class for Local Penalization acquisition. Used for batch design.
    :param model: model of the class GPyOpt
    :param space: design space of the class GPyOpt.
    :param optimizer: optimizer of the class GPyOpt.
    :param acquisition: acquisition function of the class GPyOpt
    :param transform: transformation applied to the acquisition (default, none).

    .. Note:: irrespective of the transformation applied the penalized acquisition
              is always mapped again to the log space.
    This way gradients can be computed additively and are more stable.

    """

    analytical_gradient_prediction = True

    def __init__(self, model, space, optimizer, acquisition, transform='none'):
        super(AcquisitionPureExplorationLP, self).__init__(model, space, optimizer, acquisition)

        self.acq = acquisition
        self.transform = transform.lower()
        if isinstance(acquisition, AcquisitionLCB) and self.transform == 'none':
            self.transform = 'softplus'
        if isinstance(acquisition,
                      AcquisitionLCB_MCMC) and self.transform == 'none':
            self.transform = 'softplus'

        self.X_batch = None
        self.L_mean = None
        self.L_std = None

        self.r_x0_mean = None
        self.s_x0_mean = None

        self.r_x0_std = None
        self.s_x0_std = None

    def update_batches(self, X_batch, L, Min):
        """
        Updates the batches internally and pre-computes the hammer function for penalization.
        """
        self.X_batch = X_batch
        if X_batch is not None:
            self.r_x0_mean, self.s_x0_mean, self.r_x0_std, self.s_x0_std = \
                self._hammer_function_precompute(
                    X_batch,
                    L,
                    Min,
                    self.model)
        else:
            self.r_x0_mean = None
            self.s_x0_mean = None

            self.r_x0_std = None
            self.s_x0_std = None

    def _hammer_function_precompute(self, x0, L, Min, model):
        """
        Pre-computes the parameters of a penalizer centered at x0, here x0
        can be either a vector (single x0) or a 2d np array (multiple x0).
        """
        # To prevent accidental misuse as we now set attributes L_mean and L_std.
        L = None # noqa
        if x0 is None:
            return None, None, None, None
        if len(x0.shape) == 1:
            x0 = x0[None, :]
        m, pred = model.predict(x0)
        pred = pred.copy()
        pred[pred < 1e-16] = 1e-16
        s = np.sqrt(pred)

        # ---------- GPyOpt original code for
        # r_x0 = np.abs(m-Min)/L
        # s_x0 = (np.abs(s))/L
        # ----------

        r_x0_mean = np.abs(1 - np.clip(np.abs(m), -1, 1)) / self.L_mean
        s_x0_mean = s / self.L_mean
        r_x0_mean = r_x0_mean.flatten()
        s_x0_mean = s_x0_mean.flatten()

        r_x0_std = s / self.L_std
        s_x0_std = s / self.L_std
        r_x0_std = r_x0_std.flatten()
        s_x0_std = s_x0_std.flatten()

        _logger.info("----- Entering Estimation for Penalizing Parameters -----")
        _logger.info("Pending Experiments:")
        _logger.info(x0)
        _logger.info("Penalizing Radius for pending exps:")
        _logger.info(r_x0_mean)
        _logger.info(r_x0_std)

        return r_x0_mean, s_x0_mean, r_x0_std, s_x0_std

    def _penalized_acquisition(self, x, model, X_batch,
                               r_x0_mean, s_x0_mean,
                               r_x0_std, s_x0_std):
        '''
        Creates a penalized acquisition function using 'hammer' functions around the points
        collected in the batch.

        .. Note:: the penalized acquisition is always mapped to the log space. This way gradients
                  can be computed additively and are more stable.
        '''
        fval = -self.acq.acquisition_function(x)[:, 0]

        if self.transform == 'softplus':
            fval_org = fval.copy()
            fval[fval_org >= 40.] = np.log(fval_org[fval_org >= 40.])
            fval[fval_org < 40.] = np.log(
                np.log1p(np.exp(fval_org[fval_org < 40.])))
        elif self.transform == 'none':
            fval = np.log(fval + 1e-50)

        fval = -fval
        if X_batch is not None:
            h_vals = self._hammer_function(x, X_batch, r_x0_std, s_x0_std)
            fval += -h_vals.sum(axis=-1)
        return fval

    def d_acquisition_function(self, x):
        """
        Returns the gradient of the acquisition function at x.
        """
        x = np.atleast_2d(x)

        if self.transform == 'softplus':
            fval = -self.acq.acquisition_function(x)[:, 0]
            scale = 1. / (np.log1p(np.exp(fval)) * (1. + np.exp(-fval)))
        elif self.transform == 'none':
            fval = -self.acq.acquisition_function(x)[:, 0]
            scale = 1. / fval
        else:
            scale = 1.

        if self.X_batch is None:
            _, grad_acq_x = self.acq.acquisition_function_withGradients(x)
            return scale * grad_acq_x
        else:
            _, grad_acq_x = self.acq.acquisition_function_withGradients(x)

            # dimension of grad_acq_x : (dim. of x, dim. of X_batch, dim. of parameter space).
            grad_acq_x = scale * grad_acq_x \
                - self._d_hammer_function(x, self.X_batch,
                                          self.s_x0_std, self.s_x0_std)

            # Sum over all X_batch elements.
            grad_acq_x = grad_acq_x.sum(axis=1)
            return grad_acq_x
