import GPy
from GPyOpt.models import GPModel


class Heteroscedastic(GPy.models.GPHeteroscedasticRegression):
    def predict(self, Xnew, full_cov=False, Y_metadata=None, kern=None,
                likelihood=None, include_likelihood=True):
        # Predict the latent function values
        mean, var = self._raw_predict(Xnew, full_cov=full_cov, kern=kern)
        return mean, var


class GPHeteroscedasticModel(GPModel):
    analytical_gradient_prediction = True

    def __init__(self, kernel=None, optimizer='bfgs', max_iters=1000, optimize_restarts=5,
                 num_inducing=10, verbose=True, ARD=False):
        super().__init__()
        self.kernel = kernel
        self.optimize_restarts = optimize_restarts
        self.optimizer = optimizer
        self.max_iters = max_iters
        self.verbose = verbose
        self.num_inducing = num_inducing
        self.model = None
        self.ARD = ARD

    def _create_model(self, X, Y):
        """
        Creates the model given some input data X and Y.
        """

        # --- define kernel
        self.input_dim = X.shape[1]
        if self.kernel is None:
            kern = GPy.kern.Matern52(self.input_dim, variance=1., ARD=self.ARD)
        else:
            kern = self.kernel
            self.kernel = None

        # --- define model
        self.model = Heteroscedastic(X, Y, kernel=kern)

    def updateModel(self, X_all, Y_all, X_new, Y_new, Y_var=None):
        """
        Updates the model with new observations.
        """
        if self.model is None:
            self._create_model(X_all, Y_all)
        else:
            self.model.set_XY(X_all, Y_all)

        # Setup variance for each observed Y by manually fixing the noise term. Otherwise,
        # the variance of observable would also be estimated and optimized by regular
        # GPHeteroscedasticModel.
        # self.model.Y_metadata = {'output_index': np.arange(len(Y_all))[:, None]}
        self.model['.*het_Gauss.variance'] = abs(Y_var).reshape((-1, 1))
        self.model.het_Gauss.variance.fix()

        # WARNING: Even if self.max_iters=0, the hyperparameters are bit modified...
        if self.max_iters > 0:
            # --- update the model maximizing the marginal likelihood.
            if self.optimize_restarts == 1:
                self.model.optimize(optimizer=self.optimizer,
                                    max_iters=self.max_iters,
                                    messages=False,
                                    ipython_notebook=False)
            else:
                self.model.optimize_restarts(num_restarts=self.optimize_restarts,
                                             optimizer=self.optimizer,
                                             max_iters=self.max_iters,
                                             verbose=self.verbose)
