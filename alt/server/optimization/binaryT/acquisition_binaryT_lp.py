# Modified from GPyOpt.acquisition.AcquisitionLP
# https://github.com/SheffieldML/GPyOpt/blob/master/GPyOpt/acquisitions/LP.py

from GPyOpt.acquisitions import AcquisitionBase, AcquisitionLCB, AcquisitionLCB_MCMC
from GPyOpt.util.general import samples_multidimensional_uniform
import logging
import numpy as np
import scipy
from scipy.stats import norm

_logger = logging.getLogger("BinaryT Local Penalization Acquisition")
logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)


class AcquisitionBinaryTLP(AcquisitionBase):
    """
    Class for Local Penalization acquisition. Used for batch design.
    :param model: model of the class GPyOpt
    :param space: design space of the class GPyOpt.
    :param optimizer: optimizer of the class GPyOpt.
    :param acquisition: acquisition function of the class GPyOpt
    :param transform: transformation applied to the acquisition (default, none).

    .. Note:: irrespective of the transformation applied the penalized acquisition
              is always mapped again to the log space.
    This way gradients can be computed additively and are more stable.

    """

    analytical_gradient_prediction = True

    def __init__(self, model, space, optimizer, acquisition, transform='none'):
        super(AcquisitionBinaryTLP, self).__init__(model, space, optimizer)

        self.acq = acquisition
        self.transform = transform.lower()
        if isinstance(acquisition, AcquisitionLCB) and self.transform == 'none':
            self.transform = 'softplus'
        if isinstance(acquisition,
                      AcquisitionLCB_MCMC) and self.transform == 'none':
            self.transform = 'softplus'

        self.X_batch = None
        self.L_mean = None
        self.L_std = None

        self.r_x0_mean = None
        self.s_x0_mean = None

        self.r_x0_std = None
        self.s_x0_std = None

    def update_batches(self, X_batch, L, Min):
        """
        Updates the batches internally and pre-computes the hammer function for penalization.
        """
        self.X_batch = X_batch
        if X_batch is not None:
            self.r_x0_mean, self.s_x0_mean, self.r_x0_std, self.s_x0_std = \
                self._hammer_function_precompute(
                    X_batch,
                    L,
                    Min,
                    self.model)
        else:
            self.r_x0_mean = None
            self.s_x0_mean = None

            self.r_x0_std = None
            self.s_x0_std = None

    def _hammer_function_precompute(self, x0, L, Min, model):
        """
        Pre-computes the parameters of a penalizer centered at x0, here x0
        can be either a vector (single x0) or a 2d np array (multiple x0).
        """
        # To prevent accidental misuse as we now set attributes L_mean and L_std.
        L = None # noqa
        if x0 is None:
            return None, None, None, None
        if len(x0.shape) == 1:
            x0 = x0[None, :]
        m, pred = model.predict(x0)
        pred = pred.copy()
        pred[pred < 1e-16] = 1e-16
        s = np.sqrt(pred)

        # ---------- GPyOpt original code for
        # r_x0 = np.abs(m-Min)/L
        # s_x0 = (np.abs(s))/L
        # ----------

        r_x0_mean = np.abs(1 - np.clip(np.abs(m), -1, 1)) / self.L_mean
        s_x0_mean = s / self.L_mean
        r_x0_mean = r_x0_mean.flatten()
        s_x0_mean = s_x0_mean.flatten()

        r_x0_std = s / self.L_std
        s_x0_std = s / self.L_std
        r_x0_std = r_x0_std.flatten()
        s_x0_std = s_x0_std.flatten()

        _logger.info("----- Entering Estimation for Penalizing Parameters -----")
        _logger.info("Pending Experiments:")
        _logger.info(x0)
        _logger.info("Penalizing Radius for pending exps:")
        _logger.info(r_x0_mean)
        _logger.info(r_x0_std)

        return r_x0_mean, s_x0_mean, r_x0_std, s_x0_std

    def _hammer_function(self, x, x0, r_x0, s_x0):
        '''
        Creates the function to define the exclusion zones
        '''

        # Distance between the i-th X and j-th X_0.
        dist_ij_square = (np.square(
            np.atleast_2d(x)[:, None, :] -
            np.atleast_2d(x0)[None, :, :])).sum(-1)
        dist_ij = np.sqrt(dist_ij_square)

        z = (dist_ij - r_x0) / s_x0
        hammer = norm.logcdf(z)
        return hammer

    def _penalized_acquisition(self, x, model, X_batch,
                               r_x0_mean, s_x0_mean,
                               r_x0_std, s_x0_std):
        '''
        Creates a penalized acquisition function using 'hammer' functions around the points
        collected in the batch.

        .. Note:: the penalized acquisition is always mapped to the log space. This way gradients
                  can be computed additively and are more stable.
        '''
        fval = -self.acq.acquisition_function(x)[:, 0]

        if self.transform == 'softplus':
            fval_org = fval.copy()
            fval[fval_org >= 40.] = np.log(fval_org[fval_org >= 40.])
            fval[fval_org < 40.] = np.log(
                np.log1p(np.exp(fval_org[fval_org < 40.])))
        elif self.transform == 'none':
            fval = np.log(fval + 1e-50)

        fval = -fval
        if X_batch is not None:
            selector = self._penalizer_selector(X_batch)
            h_vals = selector * self._hammer_function(x, X_batch, r_x0_mean, s_x0_mean) + \
                (1 - selector) * self._hammer_function(x, X_batch, r_x0_std, s_x0_std)
            fval += -h_vals.sum(axis=-1)
        return fval

    def _d_hammer_function(self, x, X_batch, r_x0, s_x0):
        """
        Computes the gradient of the log penalizer (centered at x_0) at any x.
        """
        dx = np.atleast_2d(x)[:, None, :] - np.atleast_2d(X_batch)[None, :, :]
        nm = np.sqrt((np.square(dx)).sum(-1))
        z = (nm - r_x0) / s_x0
        h_func = norm.cdf(z)

        d = 1. / (s_x0 * np.sqrt(2 * np.pi) * h_func) * np.exp(
            -np.square(z) / 2) / nm
        d[h_func < 1e-50] = 0.
        # original wrong code
        # d = d[:,:,None]
        # should be changed to:
        d = d[:, :, None]

        # Before this line, d : (dim. of x, dim. of X_batch, 1)
        #                   dx: (dim. of x, dim. of X_batch, dim. of parameter space).
        d = d * dx
        return d

    def acquisition_function(self, x):
        """
        Returns the value of the acquisition function at x.
        """

        return self._penalized_acquisition(x, self.model, self.X_batch,
                                           self.r_x0_mean, self.s_x0_mean,
                                           self.r_x0_std, self.s_x0_std)

    def d_acquisition_function(self, x):
        """
        Returns the gradient of the acquisition function at x.
        """
        x = np.atleast_2d(x)

        if self.transform == 'softplus':
            fval = -self.acq.acquisition_function(x)[:, 0]
            scale = 1. / (np.log1p(np.exp(fval)) * (1. + np.exp(-fval)))
        elif self.transform == 'none':
            fval = -self.acq.acquisition_function(x)[:, 0]
            scale = 1. / fval
        else:
            scale = 1.

        if self.X_batch is None:
            _, grad_acq_x = self.acq.acquisition_function_withGradients(x)
            return scale * grad_acq_x
        else:
            _, grad_acq_x = self.acq.acquisition_function_withGradients(x)
            selector = self._penalizer_selector(self.X_batch).reshape((1, -1, 1))

            # dimension of grad_acq_x : (dim. of x, dim. of X_batch, dim. of parameter space).
            grad_acq_x = scale * grad_acq_x \
                - selector * self._d_hammer_function(x, self.X_batch,
                                                     self.r_x0_mean, self.s_x0_mean) \
                - (1 - selector) * self._d_hammer_function(x, self.X_batch,
                                                           self.s_x0_std, self.s_x0_std)

            # Sum over all X_batch elements.
            grad_acq_x = grad_acq_x.sum(axis=1)
            return grad_acq_x

    def _penalizer_selector(self, X_batch):
        # Returns 1 if penalizer should be mean based, 0 if penalizer should be std based.
        m, _ = self.model.predict(X_batch)
        m = m.ravel()

        # As the mean usually within [-1, 1] to label two phases, we use 0.5 to separate phase
        # boundary with other scenarios.
        return np.abs(m) <= 0.5

    def acquisition_function_withGradients(self, x):
        """
        Returns the acquisition function and its its gradient at x.
        """
        aqu_x = self.acquisition_function(x)
        aqu_x_grad = self.d_acquisition_function(x)
        return aqu_x, aqu_x_grad

    def estimate_L(self, model, bounds, storehistory=True):
        """
        Estimate the Lipschitz constant of f by taking maximizing the norm of the expectation
        of the gradient of *f*.
        """

        def df_mean(x, model, x0):
            x = np.atleast_2d(x)
            dmdx, dvdx = model.predictive_gradients(x)
            dmdx = np.squeeze(dmdx, axis=2)

            # ---------- Original code version in GPyOpt
            # # simply take the norm of the expectation of the gradient
            # res = np.sqrt((dmdx*dmdx).sum(1))
            # ----------

            # Take the geometric mean of the mean and std's gradients, as in Acquisition BinaryT,
            # their quotient is used.
            res = np.sqrt(np.sqrt((dmdx * dmdx).sum(1)))
            res = res.reshape((-1, 1))
            return -res

        def df_std(x, model, x0):
            x = np.atleast_2d(x)
            dmdx, dvdx = model.predictive_gradients(x)
            dmdx = np.squeeze(dmdx, axis=2)
            res = np.sqrt(np.sqrt((dvdx * dvdx).sum(1)))
            res = res.reshape((-1, 1))
            return -res

        def get_L_with_function(df, ):
            pred_samples = df(samples, model, 0)
            x0 = samples[np.argmin(pred_samples)]
            res = scipy.optimize.minimize(
                df,
                x0,
                method='L-BFGS-B',
                bounds=bounds,
                args=(model, x0),
                options={'maxiter': 200},
            )
            minusL = res.fun[0][0]
            L = -minusL
            if L < 1e-7:
                L = 10  # To avoid problems in cases in which the model is flat.
            return L

        samples = samples_multidimensional_uniform(bounds, 500)
        samples = np.vstack([samples, model.X])
        self.L_mean = get_L_with_function(df_mean)
        self.L_std = get_L_with_function(df_std)
        return self.L_mean
