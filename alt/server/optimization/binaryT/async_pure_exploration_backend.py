import GPyOpt
from GPyOpt.optimization.acquisition_optimizer import AcquisitionOptimizer
import logging

from alt.server.optimization.base.async_backend_base import AsyncBackendBase, AsyncModularBO
from alt.server.optimization.binaryT.acquisition_pure_exploration import AcquisitionPureExploration
from alt.server.optimization.binaryT.acquisition_pure_exploration_lp import (
    AcquisitionPureExplorationLP)
from alt.server.optimization.binaryT.evaluator_async_lp import AsyncLocalPenalization
from alt.server.optimization.binaryT.gpmodel_heteroscedastic import GPHeteroscedasticModel


_logger = logging.getLogger("Async LCB Backend")
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)


class AsyncPureExplorationBackend(AsyncBackendBase):
    """ As a baseline to BinaryT, the pure exploration acquisition function is implemented.
    """
    EXPLORATION_WEIGHT = 10
    INIT_DESIGN_NUMDATA = 5

    def shouldStopStudy(self, studySetting, exps):
        """ returns bool """
        return NotImplementedError

    def _gen_opt_sys(self, studySetting, exp_data):
        model = GPyOpt.models.GPModel(optimize_restarts=self.OPTIMIZE_RESTARTS, verbose=False)
        acquisition_optimizer = AcquisitionOptimizer(self.space)

        pure_acquisition = AcquisitionPureExploration(
            model,
            self.space,
            acquisition_optimizer)
        lp_acquisition = AcquisitionPureExplorationLP(
            model, self.space, acquisition_optimizer, pure_acquisition)
        self.acquisition = lp_acquisition

        evaluator = AsyncLocalPenalization(lp_acquisition)
        objective = None

        opt_sys = AsyncModularBO(model, self.space, objective, lp_acquisition,
                                 evaluator, self.design, normalize_Y=False)
        opt_sys.X = exp_data.finished_X.warped
        opt_sys.Y = exp_data.finished_Y
        return opt_sys


class AsyncPureExplorationHeteroscedasticBackend(AsyncBackendBase):
    """ As a baseline to BinaryT, the pure exploration acquisition function is implemented.
    """
    EXPLORATION_WEIGHT = 10
    INIT_DESIGN_NUMDATA = 5

    def shouldStopStudy(self, studySetting, exps):
        """ returns bool """
        return NotImplementedError

    def _gen_opt_sys(self, studySetting, exp_data):
        model = GPHeteroscedasticModel(optimize_restarts=self.OPTIMIZE_RESTARTS, verbose=False)
        acquisition_optimizer = AcquisitionOptimizer(self.space)

        pure_acquisition = AcquisitionPureExploration(
            model,
            self.space,
            acquisition_optimizer)
        lp_acquisition = AcquisitionPureExplorationLP(
            model, self.space, acquisition_optimizer, pure_acquisition)
        self.acquisition = lp_acquisition

        evaluator = AsyncLocalPenalization(lp_acquisition)
        objective = None

        opt_sys = AsyncModularBO(model, self.space, objective, lp_acquisition,
                                 evaluator, self.design, normalize_Y=False)
        opt_sys.X = exp_data.finished_X.warped
        opt_sys.Y = exp_data.finished_Y
        return opt_sys
