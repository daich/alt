"""
Modified from GPyOpt.core.evaluators.batch_local_penalization
See this link
https://github.com/SheffieldML/GPyOpt/blob/master/GPyOpt/core/evaluators/batch_local_penalization.py
And publication 'Batch Bayesian optimization via local penalization' (Gonzalez et al., 2016).
Here I (Chengyu @ GlotzerLab) implement an async variation, while original was a batch method.
We take the passed in pending_X as the previous batch selected points.
"""

from GPyOpt.core.evaluators.base import EvaluatorBase
import logging

from alt.server.optimization.binaryT.acquisition_binaryT_lp import AcquisitionBinaryTLP

_logger = logging.getLogger("Async Evaluator")
logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)


class AsyncLocalPenalization(EvaluatorBase):
    """
    :param acquisition: acquisition function to be used to compute the batch.
    :param batch size: the number of elements in the batch.
    :normalize_Y: whether to normalize the outputs.
    """

    def __init__(self, acquisition):
        batch_size = 1
        super(AsyncLocalPenalization, self).__init__(acquisition, batch_size)
        self.acquisition = acquisition

    def compute_batch(self, duplicate_manager):
        """
        Computes the elements of the batch sequentially by penalizing the acquisition.
        """
        assert isinstance(self.acquisition, AcquisitionBinaryTLP)
        self.acquisition.update_batches(None, None, None)

        # If pending experiments exist, we use custom duplicate manager to
        #  pass data of pending_X and ignored_X into acquisition.
        if is_empty(duplicate_manager.pending_X):
            self.acquisition.update_batches(None, None, None)
        else:
            L = self.acquisition.estimate_L(self.acquisition.model.model,
                                            self.acquisition.space.get_bounds())
            Min = self.acquisition.model.model.Y.min()
            X_batch = extract_pending_x_from_manager(duplicate_manager)
            _logger.info("Lipschitz constant: {}".format(L))

            self.acquisition.update_batches(X_batch, L, Min)

        # Get next suggestion and then set back batch buffer to be empty,
        # so acquisition is unpenalized again.
        X_batch, acq_value = self.acquisition.optimize()
        _logger.info("Acquisition value for optimization: {}".format(acq_value))
        self.acquisition.update_batches(None, None, None)

        return X_batch


def is_empty(pending_X):
    if pending_X is None:
        return True
    else:  # Assuming pending_X is always numpy array like.
        return pending_X.size == 0


def extract_pending_x_from_manager(async_manager):
    return async_manager.pending_X
