from GPyOpt.acquisitions import AcquisitionBase


class AcquisitionPureExploration(AcquisitionBase):
    """ Modified from LCB.
    """

    analytical_gradient_prediction = True

    def __init__(self, model, space, optimizer=None):
        self.optimizer = optimizer
        super(AcquisitionPureExploration, self).__init__(model, space, optimizer)

    def _compute_acq(self, x):
        m, s = self.model.predict(x)
        f_acqu = s
        return f_acqu

    def _compute_acq_withGradients(self, x):
        """
        Computes the GP-Lower Confidence Bound and its derivative
        """
        m, s, dmdx, dsdx = self.model.predict_withGradients(x)
        f_acqu = s
        df_acqu = dsdx
        return f_acqu, df_acqu
