from alt.server import grpcext
from alt.server.logger import log_request


class RequestLoggingInterceptor(grpcext.UnaryServerInterceptor):

    def __init__(self):
        pass

    @log_request
    def intercept_unary(self, request, servicer_context, server_info, handler):
        response = handler(request, servicer_context)
        return response
