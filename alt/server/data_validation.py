from alt.server.core.external_exception import (HparamNameDuplicateError,
                                                HparamValueInvalidError,
                                                ConstraintSyntaxError)
from alt.proto.python_build.hparam_pb2 import HparamType


def hparam_setting_validate(hparamSettingList):
    # Check distinct names.
    name_set = set([hparam.name for hparam in hparamSettingList])
    if len(name_set) != len(hparamSettingList):
        raise HparamNameDuplicateError


def hparams_validate(hparamSettingList, hparamList):
    name_to_hparam = {hparam.name: hparam for hparam in hparamList}
    name_to_setting = {setting.name: setting for setting in hparamSettingList}

    if name_to_hparam.keys() != name_to_setting.keys():
        raise HparamValueInvalidError

    for name in name_to_hparam:
        single_hparam_validate(name_to_setting[name], name_to_hparam[name])


def single_hparam_validate(hparamSetting, hparam):
    """ Only supporting float at the moment.
    """
    assert hparamSetting.type == HparamType.Value('FLOAT')

    if hparam.type != hparamSetting.type:
        raise HparamValueInvalidError

    if hparam.value < hparamSetting.valrange.lower:
        raise HparamValueInvalidError

    if hparam.value > hparamSetting.valrange.upper:
        raise HparamValueInvalidError


def constraint_str_validate(string):
    if '"' in string:
        raise ConstraintSyntaxError
