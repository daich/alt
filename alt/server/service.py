from concurrent import futures
import grpc
import logging
import time

import alt.proto.python_build.service_pb2_grpc as service_pb2_grpc
from alt.server.alt_servicer import ALT


_logger = logging.getLogger("gRPC Servicer")
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)


def gen_secret_cred():
    with open('./alt-secret/tls.key', 'rb') as f:
        private_key = f.read()
    with open('./alt-secret/tls.crt', 'rb') as f:
        certificate_chain = f.read()
    server_credentials = grpc.ssl_server_credentials(((private_key,
                                                       certificate_chain),))
    return server_credentials


def main():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1))
    service_pb2_grpc.add_ALTServicer_to_server(ALT(), server)

    server.add_insecure_port('[::]:5000',)
    server.start()
    _logger.info('Server successfully started. Listening on port 5000.')

    # server.start() will not block, a sleep-loop is added to keep alive
    while True:
        time.sleep(3600 * 24)
    _logger.info('Out of infinite loop, should not be here.')


if __name__ == "__main__":
    main()
