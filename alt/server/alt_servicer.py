from functools import partial
import logging
from pymongo import MongoClient

from alt.server.interface.interface_base import safe_obj_id
from alt.server.interface.server_interfaces import (
    SvcRpcCreateStudy, SvcRpcInfoStudyToDict, SvcRpcInfoStudy, SvcRpcShouldStopStudy,
    SvcRpcCreateExperiment, SvcRpcCreateExperimentWithHparam, SvcRpcInfoExperiment,
    SvcRpcStopExperiment, SvcRpcUpdateMetric, SvcRpcFinishExperimentWithMetric,
)
from alt.proto.python_build.service_pb2_grpc import ALTServicer
from alt.server.db import ProductionMongoDB


_logger = logging.getLogger("ALT Servicer")
logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)


class ALT(ALTServicer):

    def __init__(self, db_backend=ProductionMongoDB()):
        self.db = db_backend

        STUDY_INTERFACE = [
            SvcRpcCreateStudy(self.db),
            SvcRpcInfoStudyToDict(self.db),
            SvcRpcInfoStudy(self.db),
            SvcRpcShouldStopStudy(self.db),
        ]
        EXP_INTERFACE = [
            SvcRpcCreateExperiment(self.db),
            SvcRpcCreateExperimentWithHparam(self.db),
            SvcRpcInfoExperiment(self.db),
            SvcRpcStopExperiment(self.db),
        ]
        METRIC_INTERFACE = [
            SvcRpcUpdateMetric(self.db),
            SvcRpcFinishExperimentWithMetric(self.db),
        ]

        self.rpc_interfaces = \
            STUDY_INTERFACE + EXP_INTERFACE + METRIC_INTERFACE
        for interface in self.rpc_interfaces:
            self.register_rpc(interface)

    def register_rpc(self, interface):
        # Bind a function and store it in self.
        function = interface.gen_rpc()
        setattr(self, interface.RPC_NAME, partial(function, self))

    def _exist(self, collection_name, key, val):
        # TODO: move to db_backend.
        if key == '_id':
            val = safe_obj_id(val)

        with MongoClient(self.db.ATLAS_ADDR) as client:
            collection = self.db.get_collection(client, collection_name)
            if collection.find_one({key: val}):
                ret = True
            else:
                ret = False
        return ret

    def _exist_study_by_id(self, study_id):
        return self._exist('study', '_id', study_id)

    def _exist_study_by_name(self, study_name):
        return self._exist('study', 'studyName', study_name)
