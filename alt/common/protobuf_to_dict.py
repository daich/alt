from protobuf_to_dict import protobuf_to_dict


def safe_protobuf_to_dict(pb):
    """ Introduced to solve the issue of original protobuf_to_dict missing fields that
        have value to be the same as default, as protobuf will not include it in ListFields().

        For details, see (https://github.com/protocolbuffers/protobuf/issues/1772).
    """
    dic = protobuf_to_dict(pb, including_default_value_fields=True)
    return dic
