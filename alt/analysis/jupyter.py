from bson import ObjectId
from IPython.display import display, HTML
import pymongo
import urllib

from alt.server.db import ProductionMongoDB


def display_base64_png(serialized):
    uri = 'data:image/png;base64,' + urllib.parse.quote(serialized)
    html = '<img src = "%s"/>' % uri
    display(HTML(html))


def display_exp(study_id, exp_id):
    mongo_uri = ProductionMongoDB.ATLAS_ADDR
    with pymongo.MongoClient(mongo_uri) as client:
        study_cursor = client.alt_experiments[study_id]
        exp = study_cursor.find_one({'_id': ObjectId(exp_id)})

    display_base64_png(exp['plot'])
