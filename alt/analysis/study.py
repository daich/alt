import copy
import math
import munch
from pymongo import MongoClient

from alt.proto.python_build import study_pb2
from alt.server.alt_servicer import ALT
from alt.server.core.external_exception import StudyNotFoundError
from alt.server.optimization.selector import backend_selector


class StudyAnalysis(object):
    def __init__(self, study_id):
        self.servicer = ALT()

        if not self.servicer._exist_study_by_id(study_id):
            raise StudyNotFoundError

        dummy_context = None
        request = study_pb2.StudyID(studyID=study_id)
        self.studySetting = self.servicer.infoStudy(request, dummy_context)

        with MongoClient(self.servicer.db.ATLAS_ADDR) as client:
            self.exps = self.servicer.db.get_all_exp_in_study(client, study_id)
            self.exps = list(map(munch.munchify, self.exps))

        self.opt_backend = backend_selector(self.studySetting)

    def plot_study(self, num_exp=None, pending_positions=[], normalization=dict(), plot_args=None):

        if num_exp is not None:
            if type(num_exp) is not int:
                raise TypeError()
            plot_exps = sorted(self.exps, key=lambda exp: exp['timeCreated'])
            plot_exps = self.mark_pending(plot_exps[:num_exp], pending_positions, normalization)

        _ = self.opt_backend.getNextExp(self.studySetting, plot_exps)
        self.opt_backend.plot_and_serialize(self.studySetting, plot_exps, plot_args)

    def mark_pending(self, exps, pending_positions, normalization):
        """ For each position in the `pending_positions`, it will find the closet experiment and
            mark it as pending experiment.
        """
        def distance(exp, pos):
            ret = 0
            for hparam in exp.hparamValue:
                name = hparam['name']
                ret += ((hparam['value'] - pos[name]) / normalization[name])**2
            return math.sqrt(ret)

        for pos in pending_positions:
            exps = sorted(exps, key=lambda exp: distance(exp, pos))
            exps[0] = self._mark_pending_single(exps[0])

        return exps

    def _mark_pending_single(self, exp):
        ret = copy.copy(exp)
        ret.latestMetric = None
        ret.stopped = False
        return ret
