import pytest

import alt.client.interface as interface
from alt.server.core.data import extract_hparamValue_to_dict

EPSILON = 1e-7


class TestAsyncPureExplorationWorkflow(object):
    ''' This test will test Async pure exploration workflow by always having two pending experiments.
        At any given time, there will be two experiments not finished and our service will need
        to suggest points based on both finished and pending experiments.
    '''

    def setUp(self):
        hparam_x = dict(
            name='x',
            type='FLOAT',
            valrange=dict(lower=0., upper=500.),
            warping='LINEAR')
        hparam_y = dict(
            name='y',
            type='FLOAT',
            valrange=dict(lower=0., upper=1.),
            warping='LINEAR')
        self.studySetting_dict = {
            'studyName': 'devtest_pure_exploration_async',
            'studyAuthor': 'dev',
            'maxParallelExperiments': 100,
            'maxTrials': 50,
            'goal': 'PURE_EXPLORATION',
            'hparamSetting': [hparam_x, hparam_y],
        }
        self.study_id = None
        self.exp_list = []

    def result_function(self, hparam):
        """ Phase boundary set to be (x=y), with upside value +1, downside value -1.
        """
        return int(hparam['x'] * hparam['y'] > 150) * 2 - 1

    @pytest.mark.usefixtures("grpc_stub")
    def test_binaryT_workflow(self, grpc_stub):
        self.setUp()

        self.study_id = interface.createStudyFromDict(self.studySetting_dict, test_stub=grpc_stub)

        for i in range(40):
            # Query new exp.
            exp_id = interface.createExperiment(
                self.study_id, test_stub=grpc_stub)
            exp = interface.infoExperiment(
                self.study_id, exp_id, test_stub=grpc_stub)
            self.exp_list.append(exp)

            # Run exp with index (i-2) to get result,
            # such that there is always two experiments not finished.
            if i >= 2:
                # Check that suggested new points are not closed to pending points.
                hparam_dict = extract_hparamValue_to_dict(exp)
                assert hparam_not_close(
                    hparam_dict,
                    extract_hparamValue_to_dict(self.exp_list[i - 1]))
                assert hparam_not_close(
                    hparam_dict,
                    extract_hparamValue_to_dict(self.exp_list[i - 2]))

                old_exp = self.exp_list[i - 2]
                old_hparam_dict = extract_hparamValue_to_dict(old_exp)
                result = self.result_function(old_hparam_dict)
                interface.finishExperimentWithMetric(
                    self.study_id,
                    old_exp.experimentID,
                    result,
                    test_stub=grpc_stub)

            result = extract_hparamValue_to_dict(exp)
            print(result)


def is_close(val1, val2):
    # At the moment only float are supported.
    return abs(val1 - val2) <= EPSILON


def hparam_not_close(hparam_dict_1, hparam_dict_2):
    assert hparam_dict_1.keys() == hparam_dict_2.keys()

    everything_close = True
    for k in hparam_dict_1:
        everything_close &= is_close(hparam_dict_1[k], hparam_dict_2[k])

    not_close = not everything_close
    return not_close
