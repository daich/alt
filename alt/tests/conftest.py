'''
The conftest.py file is used by Pytest to declare fixtures used in the plugins.
pytest-grpc plugin is used to generate the stub and mock service in local test machine,
such that testing does not require a full scale web service running somewhere else.

see (https://github.com/kataev/pytest-grpc/blob/master/example/test_example.py)
'''

import grpc
import pytest
from pytest_grpc.plugin import FakeChannel

from alt.server.db import TestingMongoDB


def pytest_addoption(parser):
    # The default value was needed to prevent:
    # https://github.com/pytest-dev/pytest/issues/1596
    parser.addoption("--plot_to_gui", action="store_true", default=False)


def pytest_configure(config):
    """ To let server code detect if it's called from a pytest session.
    """
    import sys
    sys._called_from_test = True
    if config.getoption("--plot_to_gui"):
        sys._pytest_opt_plot_to_gui = True


def pytest_unconfigure(config):
    import sys  # This was missing from the manual
    del sys._called_from_test


@pytest.fixture(scope='class')
def grpc_server(_grpc_server, grpc_addr, grpc_add_to_server, grpc_servicer):
    grpc_add_to_server(grpc_servicer, _grpc_server)
    _grpc_server.add_insecure_port(grpc_addr)
    _grpc_server.start()
    yield _grpc_server
    _grpc_server.stop(grace=None)


@pytest.fixture(scope='class')
def grpc_create_channel(request, grpc_addr, grpc_server):

    def _create_channel(credentials=None, options=None):
        if request.config.getoption('grpc-fake'):
            return FakeChannel(grpc_server, credentials)
        if credentials is not None:
            return grpc.secure_channel(grpc_addr, credentials, options)
        return grpc.insecure_channel(grpc_addr, options)

    return _create_channel


@pytest.fixture(scope='class')
def grpc_channel(grpc_create_channel):
    with grpc_create_channel() as channel:
        yield channel


@pytest.fixture(scope='class')
def grpc_add_to_server():
    import alt.proto.python_build.service_pb2_grpc as service_pb2_grpc

    return service_pb2_grpc.add_ALTServicer_to_server


@pytest.fixture(scope='class')
def grpc_servicer():
    from alt.server.alt_servicer import ALT

    return ALT(db_backend=TestingMongoDB())


@pytest.fixture(scope='class')
def grpc_stub(grpc_channel):
    import alt.proto.python_build.service_pb2_grpc as service_pb2_grpc

    return service_pb2_grpc.ALTStub(grpc_channel)
