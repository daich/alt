import pytest

import alt.client.interface as interface
from alt.server.core.external_exception import (
    StudyAlreadyExistError,
    ExperimentNotFoundError, ExperimentAlreadyFinishedError,
    HparamNameDuplicateError,
    IdNotValidError,
)

EPSILON = 1e-7


class TestExceptionHandling(object):

    def setUp(self):
        self.hparam_x = dict(
            name='x',
            type='FLOAT',
            valrange=dict(lower=0., upper=10.),
            warping='LINEAR')
        self.hparam_y = dict(
            name='y',
            type='FLOAT',
            valrange=dict(lower=0., upper=10.),
            warping='LINEAR')
        self.studySetting_dict = {
            'studyName': 'devtest_binaryT_async',
            'studyAuthor': 'dev',
            'maxParallelExperiments': 100,
            'maxTrials': 50,
            'goal': 'BINARYT',
            'hparamSetting': [self.hparam_x, self.hparam_y],
        }
        self.study_id = None
        self.exp_list = []

    @pytest.mark.usefixtures("grpc_stub")
    def test_study_already_exist(self, grpc_stub):
        self.setUp()

        self.study_id = interface.createStudyFromDict(
            self.studySetting_dict, test_stub=grpc_stub)

        with pytest.raises(StudyAlreadyExistError):
            self.study_id = interface.createStudyFromDict(
                self.studySetting_dict, test_stub=grpc_stub)

    @pytest.mark.usefixtures("grpc_stub")
    def test_hparam_name_duplicate_error(self, grpc_stub):
        self.setUp()

        self.studySetting_dict['studyName'] = "devtest_binaryT_async_1"
        self.studySetting_dict['hparamSetting'] = [self.hparam_x, self.hparam_x]

        with pytest.raises(HparamNameDuplicateError):
            self.study_id = interface.createStudyFromDict(
                self.studySetting_dict, test_stub=grpc_stub)

    @pytest.mark.usefixtures("grpc_stub")
    def test_exp_already_finished(self, grpc_stub):
        self.setUp()

        self.studySetting_dict['studyName'] = "devtest_binaryT_async_2"
        self.study_id = interface.createStudyFromDict(
            self.studySetting_dict, test_stub=grpc_stub)
        exp_id = interface.createExperiment(self.study_id, test_stub=grpc_stub)
        interface.finishExperimentWithMetric(
            self.study_id, exp_id, 0, test_stub=grpc_stub)

        with pytest.raises(ExperimentAlreadyFinishedError):
            interface.finishExperimentWithMetric(
                self.study_id, exp_id, 0, test_stub=grpc_stub)

    @pytest.mark.usefixtures("grpc_stub")
    def test_exp_not_found(self, grpc_stub):
        import copy

        self.setUp()

        self.studySetting_dict['studyName'] = "devtest_binaryT_async_3"
        self.study_id = interface.createStudyFromDict(
            self.studySetting_dict, test_stub=grpc_stub)
        exp_id = interface.createExperiment(self.study_id, test_stub=grpc_stub)

        wrong_exp_id = copy.copy(exp_id)
        wrong_exp_id = '0' * 10 + wrong_exp_id[10:]

        with pytest.raises(ExperimentNotFoundError):
            interface.finishExperimentWithMetric(
                self.study_id, wrong_exp_id, 0, test_stub=grpc_stub)

    @pytest.mark.usefixtures("grpc_stub")
    def test_id_not_valid(self, grpc_stub):
        self.setUp()

        self.studySetting_dict['studyName'] = "devtest_binaryT_async_5"
        self.study_id = interface.createStudyFromDict(
            self.studySetting_dict, test_stub=grpc_stub)
        exp_id = interface.createExperiment(self.study_id, test_stub=grpc_stub)

        wrong_exp_id = 'yyy'
        wrong_study_id = 'xxx'

        with pytest.raises(IdNotValidError):
            interface.finishExperimentWithMetric(
                self.study_id, wrong_exp_id, 0, test_stub=grpc_stub)
        with pytest.raises(IdNotValidError):
            interface.finishExperimentWithMetric(
                wrong_study_id, exp_id, 0, test_stub=grpc_stub)
