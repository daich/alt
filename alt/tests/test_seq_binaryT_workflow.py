import pytest

import alt.client.interface as interface
from alt.server.core.data import extract_hparamValue_to_dict


class TestSeqBinaryTWorkflow(object):

    def setUp(self):
        hparam_x = dict(
            name='x',
            type='FLOAT',
            valrange=dict(lower=0., upper=10.),
            warping='LINEAR')
        hparam_y = dict(
            name='y',
            type='FLOAT',
            valrange=dict(lower=0., upper=10.),
            warping='LINEAR')
        self.studySetting_dict = {
            'studyName': 'devtest_binaryT',
            'studyAuthor': 'dev',
            'maxParallelExperiments': 1,
            'maxTrials': 50,
            'goal': 'BINARYT',
            'hparamSetting': [hparam_x, hparam_y],
        }
        self.study_id = None
        self.exp_list = []

    def result_function(self, hparam):
        """ Phase boundary set to be (x=y), with upside value +1, downside value -1.
        """
        return int(hparam['x'] < hparam['y']) * 2 - 1

    @pytest.mark.usefixtures("grpc_stub")
    def test_opt_workflow(self, grpc_stub):
        self.setUp()

        self.study_id = interface.createStudyFromDict(
            self.studySetting_dict, test_stub=grpc_stub)

        for i in range(7):
            # Query new exp.
            exp_id = interface.createExperiment(
                self.study_id, test_stub=grpc_stub)
            exp = interface.infoExperiment(
                self.study_id, exp_id, test_stub=grpc_stub)

            # Run exp to get result.
            hparam_dict = extract_hparamValue_to_dict(exp)
            result = self.result_function(hparam_dict)

            # Upload result to server.
            interface.finishExperimentWithMetric(
                self.study_id, exp_id, result, test_stub=grpc_stub)
            self.exp_list.append(exp)

            print(extract_hparamValue_to_dict(exp))

        print(list(map(extract_hparamValue_to_dict, self.exp_list)))
