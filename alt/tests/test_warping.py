import numpy as np

from alt.server.core.warping import LinearWarping


def test_warping():
    warp = LinearWarping(0, 500, img_lower=0, img_upper=1)
    assert warp.transform(250) == 0.5
    assert warp.inverse(0.4) == 200.

    X_vector = np.array([[100], [200]])
    y_vector = np.array([[0.2], [0.4]])
    assert np.all(warp.transform(X_vector) == y_vector)
    assert np.all(warp.inverse(y_vector) == X_vector)

    assert np.all(warp.transform(X_vector.T) == y_vector.T)
    assert np.all(warp.inverse(y_vector.T) == X_vector.T)
