import numpy as np
from protobuf_to_dict import dict_to_protobuf

from alt.proto.python_build import study_pb2
from alt.server.optimization import AsyncBinaryTBackend, AsyncLcbBackend
from alt.server.core.data import RegularExpData, RegularXData, gen_warping_list


class TestSuggestNextLocations(object):
    '''
        This is the core server API of AsyncModularBO that provides the next experiments
        hparamValues (in the active phase).
    '''
    def setUp(self):
        hparam_x = dict(
            name='x',
            type='FLOAT',
            valrange=dict(lower=0., upper=10.),
            warping='LINEAR')
        hparam_y = dict(
            name='y',
            type='FLOAT',
            valrange=dict(lower=0., upper=10.),
            warping='LINEAR')
        self.studySetting_dict = {
            'studyName': 'devtest_binaryT',
            'studyAuthor': 'dev',
            'maxParallelExperiments': 1,
            'maxTrials': 50,
            'goal': 'BINARYT',
            'hparamSetting': [hparam_x, hparam_y],
        }
        self.studySetting = dict_to_protobuf(study_pb2.StudySetting,
                                             values=self.studySetting_dict)

        self.finished_X = np.array([[0, 5], [5, 2]])
        self.pending_X = np.array([[1, 9]])
        self.pending_X_1 = np.array([[2, 4]])
        self.finished_Y = np.array([[1], [-1]])

        self.warping_list = gen_warping_list(self.studySetting.hparamSetting)
        self.exp_data = RegularExpData(
            RegularXData.from_unwarped(self.pending_X, self.warping_list),
            RegularXData.from_unwarped(self.finished_X, self.warping_list),
            self.finished_Y)

    def test_estimate_L_determinism(self):
        self.setUp()
        self.binaryT = AsyncBinaryTBackend()
        self.binaryT.setup(self.studySetting, self.exp_data)
        modular_bo = self.binaryT.opt_sys

        acq = modular_bo.acquisition
        acq.model.updateModel(self.exp_data.finished_X.warped,
                              self.exp_data.finished_Y, None, None)

        np.random.seed(111)
        lipshitz_0 = acq.estimate_L(acq.model.model, acq.space.get_bounds())
        np.random.seed(999)
        lipshitz_1 = acq.estimate_L(acq.model.model, acq.space.get_bounds())
        assert np.isclose(lipshitz_0, lipshitz_1)

    def _assert_determinism(self, modular_bo):
        rand_seed_0 = 111
        suggested_0 = modular_bo.suggest_next_locations(pending_X=self.pending_X_1,
                                                        random_seed=rand_seed_0)

        rand_seed = 135
        suggested_1 = modular_bo.suggest_next_locations(pending_X=self.pending_X,
                                                        random_seed=rand_seed)
        suggested_2 = modular_bo.suggest_next_locations(pending_X=self.pending_X,
                                                        random_seed=rand_seed)
        assert np.all(suggested_1 == suggested_2)

        # We run 1-2 with a different random seed and with different pending X so that we make sure
        # there is no side effect left inside modular_bo to affect determinism and idempotency.
        suggested_3 = modular_bo.suggest_next_locations(pending_X=self.pending_X_1,
                                                        random_seed=rand_seed_0)
        assert np.all(suggested_0 == suggested_3)

    def test_determinism_binaryT_backend(self):
        self.setUp()
        self.binaryT = AsyncBinaryTBackend()
        self.binaryT.setup(self.studySetting, self.exp_data)
        binaryT = self.binaryT.opt_sys
        self._assert_determinism(binaryT)

    def test_determinism_lcb_backend(self):
        self.setUp()
        self.lcb = AsyncLcbBackend()
        self.lcb.setup(self.studySetting, self.exp_data)
        lcb = self.lcb.opt_sys
        self._assert_determinism(lcb)
