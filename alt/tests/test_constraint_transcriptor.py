import numpy as np

from alt.proto.python_build import hparam_pb2
from alt.server.optimization.constraints import ConstraintTranscriptor


def test_constraint_transcribe():
    hparamSetting = [
        hparam_pb2.HparamSetting(name='Pf', type=1, warping=1,
                                 valrange=hparam_pb2.ValueRange(lower=0, upper=1)),
        hparam_pb2.HparamSetting(name='Pe', type=1, warping=1,
                                 valrange=hparam_pb2.ValueRange(lower=0, upper=100)),
    ]
    transciptor = ConstraintTranscriptor(hparamSetting)

    inputstr = "-x['Pe'] -.5 + abs(x['Pf']) - np.sqrt(1 + x['Pf']**2)"
    # Note that X are generated from a grid of (0, 10)x(0, 10) domain, and it is to be
    # transformed into the hyperparameter's domain.
    simplified = "-x[:,1] * 10 -.5 + abs(x[:,0]/10) - np.sqrt(1 + (x[:,0]/10)**2)"
    print(transciptor.transcribe(inputstr))

    x = np.array([[1.3, 4.7], [-9, 27]])        # noqa: F841
    assert np.all(eval(simplified) == eval(transciptor.transcribe(inputstr)))
