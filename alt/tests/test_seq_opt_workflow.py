import pytest

import alt.client.interface as interface


class TestSeqOptWorkflow(object):

    def setUp(self):
        hparam_x = dict(
            name='x',
            type='FLOAT',
            valrange=dict(lower=0., upper=500.),
            warping='LINEAR')
        hparam_y = dict(
            name='y',
            type='FLOAT',
            valrange=dict(lower=0., upper=1.),
            warping='LINEAR')
        self.studySetting_dict = {
            'studyName': 'devtest_hparam_1',
            'studyAuthor': 'dev',
            'maxParallelExperiments': 1,
            'maxTrials': 50,
            'goal': 'MAXIMIZE',
            'hparamSetting': [hparam_x, hparam_y],
        }

        self.study_id = None
        self.exp_list = []

    @pytest.mark.usefixtures("grpc_stub")
    def test_opt_workflow(self, grpc_stub):
        self.setUp()

        self.study_id = interface.createStudyFromDict(
            self.studySetting_dict, test_stub=grpc_stub)

        for i in range(7):
            exp_id = interface.createExperiment(
                self.study_id, test_stub=grpc_stub)
            result = i
            interface.finishExperimentWithMetric(
                self.study_id, exp_id, result, test_stub=grpc_stub)
            exp = interface.infoExperiment(
                self.study_id, exp_id, test_stub=grpc_stub)
            self.exp_list.append(exp)
