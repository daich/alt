import pytest

import alt.client.interface as interface
from alt.server.core.external_exception import HparamValueInvalidError

EPSILON = 1e-7


class TestBasicRPC(object):

    def setUp(self):
        hparam_x = dict(
            name='x',
            type='FLOAT',
            valrange=dict(lower=0., upper=10.),
            warping='LINEAR')
        hparam_y = dict(
            name='y',
            type='FLOAT',
            valrange=dict(lower=0., upper=10.),
            warping='LINEAR')
        self.studySetting_dict = {
            'studyName': 'devtest_basic_rpc',
            'studyAuthor': 'dev',
            'maxParallelExperiments': 100,
            'maxTrials': 50,
            'goal': 'BINARYT',
            'hparamSetting': [hparam_x, hparam_y],
        }
        self.study_id = None
        self.exp_list = []

    @pytest.mark.usefixtures("grpc_stub")
    def test_createExperimentWithHparam(self, grpc_stub):
        self.setUp()
        self.studySetting_dict['studyName'] = 'devtest_basic_rpc_0'
        self.study_id = interface.createStudyFromDict(
            self.studySetting_dict, test_stub=grpc_stub)

        hparam_x_val = dict(name='x', type='FLOAT', value=0.)
        hparam_y_val = dict(name='y', type='FLOAT', value=10)
        hparam_value = [hparam_x_val, hparam_y_val]

        exp_setting_hparam = {
            'studyID': self.study_id,
            'hparamValue': hparam_value,
        }
        self.exp_id = \
            interface.createExperimentWithHparamFromDict(exp_setting_hparam,
                                                         test_stub=grpc_stub)

    @pytest.mark.usefixtures("grpc_stub")
    def test_exc_createExperimentWithHparam(self, grpc_stub):
        self.setUp()
        self.studySetting_dict['studyName'] = 'devtest_basic_rpc_1'

        self.study_id = interface.createStudyFromDict(
            self.studySetting_dict, test_stub=grpc_stub)

        with pytest.raises(HparamValueInvalidError):
            exp_setting_hparam = {
                'studyID':
                self.study_id,
                'hparamValue': [
                    dict(name='x', type='FLOAT', value=-1),
                    dict(name='y', type='FLOAT', value=10)
                ]
            }
            interface.createExperimentWithHparamFromDict(
                exp_setting_hparam, test_stub=grpc_stub)

        with pytest.raises(HparamValueInvalidError):
            exp_setting_hparam = {
                'studyID':
                self.study_id,
                'hparamValue': [
                    dict(name='x', type='FLOAT', value=11),
                    dict(name='y', type='FLOAT', value=10)
                ]
            }
            interface.createExperimentWithHparamFromDict(
                exp_setting_hparam, test_stub=grpc_stub)

        with pytest.raises(HparamValueInvalidError):
            exp_setting_hparam = {
                'studyID':
                self.study_id,
                'hparamValue': [
                    dict(name='z', type='FLOAT', value=-1),
                    dict(name='y', type='FLOAT', value=10)
                ]
            }
            interface.createExperimentWithHparamFromDict(
                exp_setting_hparam, test_stub=grpc_stub)

        with pytest.raises(HparamValueInvalidError):
            exp_setting_hparam = {
                'studyID':
                self.study_id,
                'hparamValue': [
                    dict(name='x', type='DISCRETE', value=1),
                    dict(name='y', type='FLOAT', value=10)
                ]
            }
            interface.createExperimentWithHparamFromDict(
                exp_setting_hparam, test_stub=grpc_stub)
