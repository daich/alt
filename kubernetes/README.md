# Note for CI

This dockerfile provides the docker formula for `registry.gitlab.com/daich/alt:tag`.
It will be used as the base for the whole test programs in GitLab as the base image, simply do:

``` bash
$ sudo docker build . -t registry.gitlab.com/daich/alt:tag
$ docker push registry.gitlab.com/daich/alt:tag
```
