openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ./tls.key -out ./tls.crt -subj "/CN=daich-alt.api.bidiu.me"

kubectl create secret tls alt-secret --key ./tls.key --cert ./tls.crt --namespace alt-9784806

cat tls.crt tls.key > tls.pem
