from setuptools import setup, find_packages

setup(
    name='alt',
    version='0.1.0',
    author='Regents of University of Michigan',
    author_email='daich@umich.edu',
    packages=find_packages(),
)
